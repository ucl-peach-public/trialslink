#!/usr/bin/env bash
echo "Existing rabbitmq queues"
rabbitmqadmin list queues name
echo "Deleting keytrials queues"
rabbitmqadmin delete queue name='trialslink-trials-queue'
rabbitmqadmin delete queue name='trialslink-notifications-queue'
rabbitmqadmin delete queue name='trialslink-annotations-queue'
echo "Deleted keytrials queues"
echo "Rabbitmq queues after delete"
rabbitmqadmin list queues name
