import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    SavedSearchService,
    SavedSearchPopupService,
    SavedSearchComponent,
    SavedSearchDetailComponent,
    SavedSearchDialogComponent,
    SavedSearchPopupComponent,
    SavedSearchDeletePopupComponent,
    SavedSearchDeleteDialogComponent,
    savedSearchRoute,
    savedSearchPopupRoute,
    SavedSearchResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...savedSearchRoute,
    ...savedSearchPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SavedSearchComponent,
        SavedSearchDetailComponent,
        SavedSearchDialogComponent,
        SavedSearchDeleteDialogComponent,
        SavedSearchPopupComponent,
        SavedSearchDeletePopupComponent,
    ],
    entryComponents: [
        SavedSearchComponent,
        SavedSearchDialogComponent,
        SavedSearchPopupComponent,
        SavedSearchDeleteDialogComponent,
        SavedSearchDeletePopupComponent,
    ],
    providers: [
        SavedSearchService,
        SavedSearchPopupService,
        SavedSearchResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkSavedSearchModule {}
