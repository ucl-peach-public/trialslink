import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SavedSearch } from './saved-search.model';
import { SavedSearchService } from './saved-search.service';

@Component({
    selector: 'jhi-saved-search-detail',
    templateUrl: './saved-search-detail.component.html'
})
export class SavedSearchDetailComponent implements OnInit, OnDestroy {

    savedSearch: SavedSearch;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private savedSearchService: SavedSearchService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSavedSearches();
    }

    load(id) {
        this.savedSearchService.find(id).subscribe((savedSearch) => {
            this.savedSearch = savedSearch;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSavedSearches() {
        this.eventSubscriber = this.eventManager.subscribe(
            'savedSearchListModification',
            (response) => this.load(this.savedSearch.id)
        );
    }
}
