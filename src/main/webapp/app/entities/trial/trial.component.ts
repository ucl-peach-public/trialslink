import { Component, OnInit, OnDestroy, ViewContainerRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import {Location} from '@angular/common';
import { Subscription, Subject } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
import { concat } from 'rxjs/observable/concat';
//import { map } from 'rxjs/operators';
import { map, distinctUntilChanged, debounceTime, switchMap, tap, catchError } from 'rxjs/operators'
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import {LocalStorageService, LocalStorage} from 'ngx-webstorage';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { TermlexService } from 'termlex-ng';

import { Trial } from './trial.model';
import { Category } from './category.model';
import { Query } from './query.model';
import { TrialService } from './trial.service';
import { ConceptService } from '../concept/concept.service';
import { OrganisationService } from '../organisation/organisation.service';
import { InfoService, ITEMS_PER_PAGE, LoginService, Principal, ResponseWrapper} from '../../shared';
import * as _ from 'underscore';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import {SavedSearch} from "../saved-search/saved-search.model";
import {SavedSearchService} from "../saved-search/saved-search.service";
import {Observable} from "rxjs/Observable";
import {FormControl, FormGroup, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';

@Component({
    selector: 'jhi-trial',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './trial.component.html'
})
export class TrialComponent implements OnInit, OnDestroy {

    loginSubscription: Subscription;
    trials: Trial[];
    categories: Category[];
    query: Query;
    previousQuery: Query;
    currentAccount: any;
    eventSubscriber: Subscription;
    locationSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    currentSearch: string;
    currentTokens: Array<Object> = [];
    public conditions: Array<any>;
    public locations: Array<any>;
    conditionsLookup: any;
    locationsLookup: any;
    knownStatuses: any;
    categoryKeyLookup: any;
    semanticConditionsLookup: Array<any>;
    private value: any = {};
    lookup: any;
    phaseLookup: any;
    params: URLSearchParams = new URLSearchParams();
    showSmallScreenFilters: boolean = false;
    unlockFilters: boolean = true;
    private tempMap: any = {};
    selectedConditions: Array<any>;
    selectedLocations: Array<any>;
    isOmniSearch: boolean;
    isSearchQuery: boolean;
    savedSearchSuccess: boolean;
    savedSearchError: boolean;
    useMaterial: boolean = true;
    inProgress: boolean = true;
    noTrials: boolean = false;
    model: any;
    brand: string;
    preferOmnisearch: boolean;
    useLocalStorage: boolean;
    allowedFilters: Array<any>;
    singleSiteInstance: boolean;
    showSitesFilter:  boolean;
    cities = [
        {id: 1, display: 'Vilnius'},
        {id: 2, display: 'Kaunas'},
        {id: 3, display: 'Pavilnys', disabled: true},
        {id: 4, display: 'Pabradė'},
        {id: 5, display: 'Klaipėda'}
    ];
    searchResults = new Subject<string>();
    searchResultsLoading: boolean = false;
    dropDownConditions: Array<any> = [];
    showSemanticSearch: boolean = false;
    useInfiniteScroll: boolean = false;
    termlexConditions: any[] = [];
    conditionsLoading = false;
    conditionsTypeahead = new Subject<string>();
    selectedSemanticSearch: any[] = <any>[];
    includeAncestors: boolean;
    constructor(
        private trialService: TrialService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private http: Http,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private conceptService: ConceptService,
        private organisationService: OrganisationService,
        private savedSearchService: SavedSearchService,
        private location: Location,
        private localStorageService:LocalStorageService,
        private toastsManager:ToastsManager,
        private vcr: ViewContainerRef,
        private loginService: LoginService,
        private infoService: InfoService,
        private cd: ChangeDetectorRef
    ) {
        this.includeAncestors = false;
        this.trials = [];
        this.categories = [];
        this.query = new Query();
       // this.query.includeAncestors = false;
        this.previousQuery = new Query();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'lastModifiedDate';
        this.reverse = false;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.lookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV'};
        this.isOmniSearch = false;
        this.isSearchQuery = false;
        this.phaseLookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV',
            zero: 'Phase 0', one: 'Phase I', two: 'Phase II', three: 'Phase III',  four: 'Phase IV'};
        this.categoryKeyLookup = {phases: 'Phase', statuses: 'Trial status', siteStatuses: 'Trial status', types: 'Trial Type', locations: 'Location',
            genders: 'Gender', conditionStatuses: 'Condition status', sites: 'Disease site', areas: 'Disease category'};
        //this.allowedFilters = ['phases', 'statuses', 'siteStatuses', 'types', 'areas'];
        this.toastsManager.setRootViewContainerRef(vcr);
        this.semanticConditionsLookup = [];
        this.initConditionsAndLocations();
        this.registerChangeInTrials();
        // register a subscriber that listens to router events and translates them into searches
        this.locationSubscriber = this.router.events.subscribe((event) => {
            //console.log("event  = " , event );
            if(event instanceof NavigationEnd) {
                console.log("event.url  = " , event.url );
                // update search state
                this.dropDownConditions = [];
                this.updateSearchState();
            }
        });
        this.loginSubscription = this.loginService.logStatus().subscribe(value => {
            this.updateCurrentAccount();
        });
        this.infoService.getBrand().subscribe(brand => {
            this.brand = brand._body;
        });
        this.infoService.preferOmnisearch().subscribe(value => {
            this.preferOmnisearch = value;
        });
        this.infoService.useLocalStorage().subscribe(value => {
            this.useLocalStorage = value;
        });
        //this.termlexService = new TermlexService(this.http, {
        //    //token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1NzczMjQzOX0.4THAACWt7xHpbVofLD3ILaLo1bIeSAR_4lWq5AGbYBRDI1lvGeU-CxRTx8Okn7-I8DY9g5k6rKAnZB6OPVBkYQ',
        //    token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1NzgxMjY0OX0.bINrUzT7crjVeXi8EFOqEYNfhZo0OsVRwgUWS1UVt-4dDsjheQanLqkffid0W8qcJaay_rJqw1vB7Dz2yY-xxg',
        //    url: 'https://uat.termlex.com',
        //    resultOpts: {
        //        maxInstantResultsSize: 100,  // max number of instant results
        //        conceptType: "919191919191441993", // conceptId of SNOMED CT top level concept - returns concepts in all hierarchies
        //        maxResultsSize: 100
        //    }
        //});
    }


        ngOnInit() {

            this.infoService.getSettings().subscribe(value => {
                this.singleSiteInstance = value.singleSiteInstance;
                console.log("value  = " , value );
                this.allowedFilters = value.allowedFilters;
                // assigning an attribute which can tell if the disease site filter is desired as default or not
                if (this.allowedFilters.indexOf('sites') > -1){
                    this.showSitesFilter = true;
                }

            });
            // if allowed parameters is null of undefined, we set default values.
            if(!this.allowedFilters) {
                this.allowedFilters = ['phases', 'statuses', 'types', 'areas'];
            }
            console.log("this.allowedFilters  = " , this.allowedFilters );
            this.loadConditions();
            // load user identity info
            this.updateCurrentAccount();
          //  this.flag= this.query.includeAncestors;
        }


     /*includeAncestors = (e: any):boolean => {
        this.query.includeAncestors = e.target.checked;
             this.updateRoute();
     }*/

     addAncestors(e){
     console.log("Event = ",e.checked);
     this.query.includeAncestors = e.checked;
     this.updateRoute();
     }

    updateCurrentAccount(){
        // load user identity info
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    selectedCondition(value: any): void {
        this.query.conditions.push(value.id);
        // set unlock filters to true
        this.unlockFilters = true;
        this.updateRoute();
    }



    onConditionItemAdded($event: any): void {
        console.log("$event  = " , $event );
        this.query.conditions.push($event.id+'|'+$event.label);
        // set unlock filters to true
        this.unlockFilters = true;
        this.updateRoute();
    }

    clearCondtions() {
        this.query.conditions = [];
        this.unlockFilters = true;
        this.updateRoute();
    }

    selectedLocation(value: any): void {
        this.query.locations.push(value.id);
        // set unlock filters to true
        this.unlockFilters = true;
        this.updateRoute();
    }

    removedCondition(value: any): void {
        this.query.conditions = this.query.conditions.filter(function(v){return v !== value.id; });
        // set unlock filters to true
        this.unlockFilters = true;
        this.updateRoute();
    }

    onConditionItemRemoved($event: any): void {
        console.log("$event  = " , $event );
        this.query.conditions = this.query.conditions.filter(function(v){
            console.log("v  = " , v );
            return !v.includes('|'+$event.label);
        });
        // set unlock filters to true
        this.unlockFilters = true;
        this.updateRoute();
    }

    removedLocation(value: any): void {
        this.query.locations = this.query.locations.filter(function(v){return v !== value.id; });
        this.updateRoute();
    }

    omniSearch(value:any):void {
        // just pass to search as the omnisearch field is bound to this.query.token property - it will be included as parameter
        this.search();
    }

    setSortIcon():string {
        if(this.reverse){
            return 'fa fa-sort-amount-desc';
        } else {
            return 'fa fa-sort-amount-asc'
        }
    }

    loadAll () {
        // update state to show spinner
        //this.inProgress = true;
        this.resetSavedSearch();
        console.log("this.query  = " , this.query );
        // if there are any query parameters filled in then do a search, otherwise just load all trials
        if (this.query.conditions.length > 0 || this.query.locations.length > 0 || this.query.age > 0
                || this.query.genders.length > 0 || this.query.conditionStatuses.length > 0 || this.query.phases.length > 0
                || this.query.statuses.length > 0 || (this.query.token && this.query.token.length > 2)
                || this.query.types.length > 0 || this.query.areas.length > 0 || this.query.sites.length > 0 || this.query.siteStatuses.length > 0 || this.query.includeAncestors) {
            // if have conditions or we prefer omniSearch, we execute just that first to generate all relevant filters - conditionOnlySearch
            // see rationale in onSuccess() function for why this happens -- //
            // we need to chain calls - so only after condition only search is completed, we want to rerun search again
            // for this reason, we repeat  call to 'search' in the 'onNext' callback of subscribe event
            if(this.query.conditions.length > 0 || this.preferOmnisearch){
                this.isSearchQuery = true;
                let conditionsOnlyQuery = new Query();
                conditionsOnlyQuery.conditions = this.query.conditions;
                if (this.preferOmnisearch) {
                    conditionsOnlyQuery.token = this.query.token;
                }
                this.unlockFilters = true;
                this.trialService.search({
                    query: conditionsOnlyQuery,
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: Response) => this.onSuccess(res.json(), res.headers, true),
                    (res: Response) => this.onError(res.json()),
                    () => this.trialService.search({
                        query: this.query,
                        page: this.page,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json(), res.headers, false),
                        (res: ResponseWrapper) => this.onError(res.json())
                    )
                );
            } else {
                this.unlockFilters = true;
                console.log('No condition selected and preferOmniSearch is false');
                this.trialService.search({
                    query: this.query,
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: Response) => this.onSuccess(res.json(), res.headers, false),
                    (res: Response) => this.onError(res.json())
                );
            }
        } else {
            // load all trials
            this.unlockFilters = true;
            this.trialService.query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: Response) => this.onSuccess(res.json(), res.headers, false),
                (res: Response) => this.onError(res.json())
            );
        }
    }

    reset() {
        this.page = 0;
        this.trials = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    initConditionsAndLocations() {
        if (this.useLocalStorage) {
            this.knownStatuses = this.localStorageService.retrieve('statuses');
        }
        if(!this.knownStatuses) {
            this.conceptService.statusesAsFilter({
                    size: 20
                }
            ).subscribe(
                (res: Response) => {
                    this.knownStatuses = _.indexBy(res.json(), 'id');
                    // store in localStorage
                    if (this.useLocalStorage) {
                        this.localStorageService.store('statuses', this.knownStatuses);
                    }
                },
                (res: Response) => this.onError(res.json())
            );
        }

        if (this.useLocalStorage) {
            // load conditions from local storage or server if needed
            this.conditions = this.localStorageService.retrieve('conditions');
            this.conditionsLookup = this.localStorageService.retrieve('conditionsLookup');
        }
        if(!this.conditions || !this.conditionsLookup) {
            this.conceptService.allAsFilter({
                    size: 500
                }
            ).subscribe(
                (res: Response) => {
                    this.conditions = res.json().filter(function(c){return c.type !== 'status'});
                    this.conditions = _.sortBy(this.conditions, function(c) {
                        return c.text;});
                    let allConditions = this.conditions;
                    this.conditionsLookup = _.indexBy(allConditions, 'id');
                    // store in local storage
                    if (this.useLocalStorage) {
                        this.localStorageService.store('conditions', this.conditions);
                        this.localStorageService.store('conditionsLookup', this.conditionsLookup);
                    }
                },
                (res: Response) => this.onError(res.json())
            );
        }

        if (this.useLocalStorage) {
            // retrieve organisations from local storage
            this.locations = this.localStorageService.retrieve('locations');
            this.locationsLookup = this.localStorageService.retrieve('locationsLookup');
        }
        if(!this.locations || !this.locationsLookup) {
            this.organisationService.allAsFilter({
                    size: 500
                }
            ).subscribe(
                (res: Response) => {
                    this.locations = res.json();
                    this.locations = _.sortBy(this.locations, function(l) {return l.text;});
                    this.locationsLookup = _.indexBy(this.locations, 'id');
                    // save to local storage
                    if (this.useLocalStorage) {
                        this.localStorageService.store('locations', this.locations);
                        this.localStorageService.store('locationsLookup', this.locationsLookup);
                    }
                },
                (res: Response) => this.onError(res.json())
            );
        }

    }

    updateSearchState() {
        // try to set query from the search query param - url will look like /trial?search=xxxx where xxx is search token
        Object.keys(this.activatedRoute.snapshot.queryParams).forEach(key => {
            var value = this.activatedRoute.snapshot.queryParams[key];
            if(key === 'token') {
                this.query.token = this.activatedRoute.snapshot.queryParams['token'] ? this.activatedRoute.snapshot.queryParams['token'] : '';
            }
            else if(key === 'age') {
                this.query.age = value;
            }
            else if (key === 'age.v.v') {
                this.query.age = value;
            }
            else if(key === 'includeAncestors') {
                 this.query.includeAncestors = value;
            }
            else if (key.includes('mainSearchCriteria.v.c')){
                this.query.conditions.push(value);
            }
            else if (this.query[key]) {
                // process as possible list of items
                console.log("value  = " , value );
                let parts: Array<string>;
                if(!Array.isArray(value)){
                    parts = value.split(",");
                } else {
                    parts = value;
                }
                // now process parts
                console.log("parts  = " , parts );
                if(parts.length > 0){
                    parts.forEach(part => {
                        this.query[key].push(part);
                    });
                    // make array unique
                    this.query[key] = _.unique(this.query[key]);
                } else {
                    this.query[key] = value;
                }
            }
            if (key === 'conditions'){
                this.showSemanticSearch = true;
                // We are using split function below to get the condition name from query.conditions array
                // which has both concept code and its name (#408 issue https://gitlab.com/keytrials/trialslink/issues/408)
                this.query.conditions.forEach(condition => {
                    let splitArray : any;
                    splitArray = condition.split('|');
                    this.dropDownConditions.push(splitArray[1]);
                });
            }
        });

        // now load all conditions and get ones specified in url query params as selectedConditions
        // todo: need to save all conditions in localStorage - wasteful to do a remote call for each load
        // now extract a selected conditions array for display in conditions drop down
        let queryConditions = this.query.conditions;
        let allConditions = this.conditions;
        this.selectedConditions = _.filter(allConditions, function(c:any) {
            return _.contains(queryConditions, c.id)
        });

        // now load all locations and process locations specified in query params
        // todo: need to save all locations in localStorage - wasteful to do a remote call for each load
        var queryLocations = this.query.locations;
        this.selectedLocations = _.filter(this.locations, function(c:any) {
            return _.contains(queryLocations, c.id)
        });

        // now execute search
        this.search();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.eventManager.destroy(this.locationSubscriber);
    }

    trackId(index: number, item: Trial) {
        return item.id;
    }

    updateFilters(key: any, value: any) {
        // process based on key and if value is present we remove, otherwise we add.
        if(key == 'conditionStatuses'){
            if(_.contains(this.query.conditionStatuses, value)){
                this.query.conditionStatuses = this.query.conditionStatuses.filter(function(v){return v != value;});
            } else {
                this.query.conditionStatuses.push(value);
            }
        } else if(key == 'phases'){
            if(_.contains(this.query.phases, value)){
                this.query.phases = this.query.phases.filter(function(v){return v != value;});
            } else {
                this.query.phases.push(value);
            }
        } else if(key == 'types'){
            if(_.contains(this.query.types, value)){
                this.query.types = this.query.types.filter(function(v){return v != value;});
            } else {
                this.query.types.push(value);
            }
        } else if(key == 'statuses'){
            if(_.contains(this.query.statuses, value)){
                this.query.statuses = this.query.statuses.filter(function(v){return v != value;});
            } else {
                this.query.statuses.push(value);
            }
        } else if(key == 'siteStatuses'){
            if(_.contains(this.query.siteStatuses, value)){
                this.query.siteStatuses = this.query.siteStatuses.filter(function(v){return v != value;});
            } else {
                this.query.siteStatuses.push(value);
            }
        } else if(key == 'genders'){
            if(_.contains(this.query.genders, value)){
                this.query.genders = this.query.genders.filter(function(v){return v != value;});
            } else {
                this.query.genders.push(value);
            }
        } else if(key == 'locations'){
            if(_.contains(this.query.locations, value)){
                this.query.locations = this.query.locations.filter(function(v){return v != value;});
            } else {
                this.query.locations.push(value);
            }
        } else if (key == 'areas') {
            if(_.contains(this.query.areas, value)){
                this.query.areas = this.query.areas.filter(function(v){return v != value;});
            } else {
                this.query.areas.push(value);
            }
        } else {
            if(_.contains(this.query.sites, value)){
                this.query.sites = this.query.sites.filter(function(v){return v != value;});
            } else {
                this.query.sites.push(value);
            }
        }

        // execute search
        this.updateRoute();
    }

    setCheckState(key:any, value:any) {
        /*// if key is 'area' or 'site' we need to change it to 'metas' as query model treats both as metas property
        if(key === 'areas' || key === 'sites') {
            key = 'metas';
        }*/
        // verify if given property in query contains value - this sets checked status of filter checkbox
        return _.contains(this.query[key], value);
    }

    indexAllTrials() {
        this.trialService.indexAllTrials().subscribe(
            (res: Response) => this.alertService.success('Indexed successfully', null, null),
            (res: Response) => this.alertService.error(res.json().message, null, null)
        );
    }

    requestAutocompleteItems = (text: string): Observable<Response> => {

        console.log("text  = " , text );

        let searchOpts:{ [key:string]: any; } = {};
        searchOpts.maxInstantResultsSize = 10;
        searchOpts.conceptType = ['404684003', '64572001'];
        searchOpts.maxResultsSize = 10;
        searchOpts.term = text;
        console.log("searchOpts  = " , searchOpts );
        //this.conceptService.searchSct(searchOpts).subscribe(
        //    (res: Response) => {
        //        console.log("res  = " , res );
        //        console.log("res json  = " , res.json() );
        //        this.dropDownConditions = res.json().results;
        //    },
        //    (res: Response) => {
        //        console.log("Error res  = " , res );
        //        this.alertService.error(res.json().message, null, null)
        //    }
        //);

        //return this.conceptService.searchSct(searchOpts)
        //    .map(data => data.json().results);

        console.log("this.query.conditions  = " , this.query.conditions );
        return this.conceptService.searchSct(searchOpts)
            .pipe(map(data => data.json().results
                .map((resultItem: any) => {
                    let item = { display: resultItem.label, value: resultItem.id };
                    let condition = { text: resultItem.label, id: resultItem.id, type: 'condition' };
                    this.semanticConditionsLookup.push(condition);
                    return item;
                })));
    };

    termlexSearch($event:any) {

        console.log("$event  = " , $event );

        let searchOpts:{ [key:string]: any; } = {};
        searchOpts.maxInstantResultsSize = 10;
        searchOpts.conceptType = ['404684003', '64572001'];
        searchOpts.maxResultsSize = 10;
        searchOpts.term = $event;
        console.log("searchOpts  = " , searchOpts );

        console.log("this.query.conditions  = " , this.query.conditions );
        return this.conceptService.searchSct(searchOpts)
            .pipe(map(data => data.json().results
                .map((resultItem: any) => {
                    let item = { display: resultItem.label, value: resultItem.id };
                    return item;
                })));
    }

    //private loadPeople3() {
    //
    //    this.people3$ = concat(
    //        of([]), // default items
    //        this.people3input$.pipe(
    //            debounceTime(200),
    //            distinctUntilChanged(),
    //            tap(() => this.people3Loading = true),
    //            switchMap(term => this.conceptService.searchSct(term).pipe(
    //                catchError(() => of([])), // empty list on error
    //                tap(() => this.people3Loading = false)
    //            ))
    //        )
    //    );
    //}

    private loadConditions() {
        this.conditionsTypeahead.pipe(
            tap(() => this.conditionsLoading = true),
            distinctUntilChanged(),
            debounceTime(200),
            switchMap(term => this.conceptService.searchSct(term)),
        ).subscribe(x => {
                console.log("x  = " , x );
                console.log("x.json()  = " , x.json() );
                this.termlexConditions = x.json().results;
                this.conditionsLoading = false;
                this.cd.markForCheck();
            }, () => {
                this.termlexConditions = [];
            });
    }

    clear () {
        this.trials = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'lastModifiedDate';
        this.reverse = false;
        this.currentSearch = '';
        this.loadAll();
    }

    updateRoute() {
        //this.inProgress = true;
        // convert query object to map to pass as query params to route
        this.tempMap = {};
        Object.keys(this.query).forEach(key => {
            if(key === 'token') {
                if (this.query.token && this.query.token.length > 2) {
                    this.tempMap['token'] = this.query.token;
                }
            }
            else if(key === 'includeAncestors'){
                  this.tempMap['includeAncestors'] = this.query.includeAncestors;
             }
            else if(key === 'age') {
                this.tempMap['age'] = this.query.age;
            }
            else if(this.query[key] && this.query[key].length > 0){
                this.tempMap[key] = this.query[key];
            }
        });
        this.router.navigate(['/trial'], { queryParams: this.tempMap });
    }

    search () {
        // reset existing parameters and use loadAll to perform search - or load all trials as needed
        this.trials = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        // this.predicate = '_score';
        this.reverse = true;
        // this.currentSearch = query;
        this.loadAll();
    }

    registerChangeInTrials() {
        this.eventSubscriber = this.eventManager.subscribe('trialListModification', (response) => this.reset());
    }

    updatedPredicate(predicate: any) {
        this.trials = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = predicate;
        this.loadAll();
    }

    sortPredicate() {
        this.reverse = !this.reverse;
        this.updatedPredicate(this.predicate);
    }

    sort () {
        if (this.predicate == null) {
            this.predicate = 'lastModifiedDate';
        }
        let result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        return result;
    }

    resetSavedSearch() {
        this.isOmniSearch = false;
        this.isSearchQuery = false;
        this.savedSearchSuccess = false;
        this.savedSearchError = false;
    }

    saveSearch() {
        let savedSearch: SavedSearch = new SavedSearch();

        savedSearch.shouldNotify = true;
        savedSearch.title = this.generateSearchTitle();
        if (this.isSearchQuery) {
            savedSearch.queryModel = this.query;
        }
        if (this.isOmniSearch) {
            savedSearch.omniSearchString = this.currentSearch;
        }
        savedSearch.url = window.location.href;
        this.subscribeToSavedSearchResponse(
            this.savedSearchService.create(savedSearch)
        );
    }

    private generateSearchTitle():string {
        let searchTitle:string = "Saved search ";
        if (this.isOmniSearch) {
            searchTitle = searchTitle + "FOR " + this.currentSearch;
        }
        if (this.isSearchQuery) {
            let conditionSearchString:string = this.generateConditionSearchString();
            searchTitle = searchTitle + "FOR " + conditionSearchString;

            let locationSearchString:string = this.generateLocationSearchString();
            searchTitle = searchTitle + "\nAT " + locationSearchString;

            let phaseSearchString:string = this.generatePhaseSearchString();
            searchTitle = searchTitle + "\nPHASES: " + phaseSearchString;

            let genderSearchString:string = this.generateGenderSearchString();
            searchTitle = searchTitle + "\nGENDER: " + genderSearchString;

            let diseaseAreaSearchString:string = this.generateAreaSearchString();
            searchTitle = searchTitle + "\nDISEASE AREA: " + diseaseAreaSearchString;

            let diseaseSiteSearchString:string = this.generateSiteSearchString();
            searchTitle = searchTitle + "\nDISEASE SITE: " + diseaseSiteSearchString;

            if (this.query.age > 0) {
                searchTitle = searchTitle + "\nAGE: " + this.query.age;
            }

            //TODO: handle (this.query.conditionStatuses.length > 0 || this.query.statuses.length > 0)
        }
        return searchTitle;
    }

    private generateConditionSearchString():string {
        /*
            We already have saved all conditions in local storage, which is connected to `semanticConditionsLookup` variable
            So we just do a lookup of all conditions to pull out the text for each concept id associated in conditions.
         */
        let conditionSearchString = "";
        for (let conditionId of this.query.conditions) {
            let matchedCondition: any = this.semanticConditionsLookup.filter(function(v){return v.id == conditionId;} );
            let conditionText = "`" + matchedCondition['text'] + "`";
            if (conditionSearchString === "") {
                conditionSearchString = conditionText;
                continue;
            }
            conditionSearchString = conditionSearchString + " OR " + conditionText;
        }
        return conditionSearchString === "" ? "ALL TRIALS" : conditionSearchString;
    }

    private generateAreaSearchString():string {
        let diseaseAreaSearchString = "";
        for (let id of this.query.areas) {
            let diseaseAreaText = "`" + this.conditionsLookup[id].text + "`";
            if (diseaseAreaSearchString === "") {
                diseaseAreaSearchString = diseaseAreaText;
                continue;
            }
            diseaseAreaSearchString = diseaseAreaSearchString + " OR " + diseaseAreaText;
        }
        return diseaseAreaSearchString === "" ? "ALL DISEASE AREAS" : diseaseAreaSearchString;
    }

    private generateSiteSearchString():string {
        let generateSiteSearchString = "";
        for (let id of this.query.sites) {
            let diseaseSiteText = "`" + this.conditionsLookup[id].text + "`";
            if (generateSiteSearchString === "") {
                generateSiteSearchString = diseaseSiteText;
                continue;
            }
            generateSiteSearchString = generateSiteSearchString + " OR " + diseaseSiteText;
        }
        return generateSiteSearchString === "" ? "ALL DISEASE SITES" : generateSiteSearchString;
    }

    private generateLocationSearchString():string {
        let locationSearchString = "";
        for (let locationId of this.query.locations) {
            let locationText = "`" + this.locationsLookup[locationId].text + "`";
            if (locationSearchString === "") {
                locationSearchString = locationText;
                continue;
            }
            locationSearchString = locationSearchString + " OR " + locationText;
        }
        return locationSearchString === "" ? "ALL LOCATIONS" : locationSearchString;
    }

    private generatePhaseSearchString():string {
        let phaseSearchString = "";
        for (let phase of this.query.phases) {
            let phaseText = "`" + this.phaseLookup[phase] + "`";
            if (phaseSearchString === "") {
                phaseSearchString = phaseText;
                continue;
            }
            phaseSearchString = phaseSearchString + " | " + phaseText;
        }
        return phaseSearchString === "" ? "ALL PHASES" : phaseSearchString;
    }

    private generateGenderSearchString():string {
        let genderSearchString = "";
        for (let gender of this.query.genders) {
            let genderText = "`" + gender + "`";
            if (genderSearchString === "") {
                genderSearchString = genderText;
                continue;
            }
            genderSearchString = genderSearchString + " | " + genderText;
        }
        return genderSearchString === "" ? "ALL GENDERS" : genderSearchString;
    }

    subscribeToSavedSearchResponse(result: Observable<SavedSearch>) {
        result.subscribe(
            (res: SavedSearch) => this.onSuccessfulSavedSearch(res),
            (res: Response) => this.onFailedSavedSearch(res)
        );
    }

    onSuccessfulSavedSearch(savedSearch: SavedSearch) {
        this.savedSearchSuccess = true;
        this.toastsManager.success('Search saved', 'Success!');
        return;
    }

    onFailedSavedSearch(res: Response) {
        this.savedSearchError = true;
        this.toastsManager.error('Search not saved', 'Error!');
        return;
    }

    private onSuccess(data, headers, conditionOnlySearch:boolean) {

        /*
           the boolean conditionOnlySearch is a flag that determines if we process results for display or if we only
           use results to generate categories/filters. This is due to an odd requirement we need when we instantiate
           the view purely from a url. For example, when a url like:
           http://localhost:9000/#/trial?conditions=59418cd97059d9d7c5f23de9&conditionStatuses=59418cdb7059d9d7c5f23e2b
           is passed, what we want to do is show all possible filters for the condition - 59418cd97059d9d7c5f23de9
           This will mean there are more than one condition statuses available as filters. At a second pass, we set
           conditionStatuses - 59418cdb7059d9d7c5f23e2b to 'checked'
         */
        if(!conditionOnlySearch) {
            this.links = this.processLinks(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');
            console.log("this.totalItems  = " , this.totalItems );

            for (let i = 0; i < data.results.length; i++) {
                this.trials.push(data.results[i]);
            }
        }
        if((data.results !== undefined && data.results.length > 0) || (data !== undefined && data.length > -1)) {


            // set categories
            if(this.unlockFilters) {
                let v = [];
                // feat:286 disease site filter should apprea only when cancer in disease area is selected only for uclh instance
                if (!this.showSitesFilter && this.brand === 'uclh') {
                    let cancerId: any;
                    this.query.areas.forEach(value => {
                        if ('area' == this.conditionsLookup[value].type && 'Cancer' == this.conditionsLookup[value].text) {
                            cancerId = value;
                        }
                    });
                    let index = this.allowedFilters.indexOf('sites');
                    if (this.query.areas.indexOf(cancerId) > -1) {
                        if (index < 0) {
                            this.allowedFilters.push('sites');
                        }
                    } else {
                        if (index > -1) {
                            this.allowedFilters = _.without(this.allowedFilters, 'sites')
                        }
                    }
                }
                let filters = this.allowedFilters;
                let conditionsLookup = this.conditionsLookup;
                // if this is a single site instance, we can remove trial status category, as we only need trial site status
                if(this.singleSiteInstance) {
                    filters = _.without(filters, 'statuses');
                } else {
                    filters = _.without(filters, 'siteStatuses');
                }
                _.mapObject(data.categories, function (value, key) {
                    if(key != 'conditions' && _.contains(filters, key)) {
                        let category = new Category();
                        category.key = key;
                        // sort value by type, so we can have predictable order of category items to select
                        if(key == 'areas') {
                            value = _.sortBy(value, function(item){return conditionsLookup[item['type']].text;});
                        } else {
                            value = _.sortBy(value, 'type');
                        }
                        category.items = value;
                        v.push(category);
                    }
                });
                // sort categories themselves for display
                v = _.sortBy(v, 'key');
                this.categories = v;
                // now lock filters after categories have been updated
                this.unlockFilters = false;
            }

            // client side filtering for unique items - work around for #134
            this.trials = _.uniq(this.trials, function (item, key, a) {return item.id});
            // do some client side sorting - workaround for bug #105
            this.trials = this.trials.sort((t1:Trial, t2:Trial) => {
                if(t1[this.predicate] > t2[this.predicate]) {
                    if(this.reverse){
                        return -1;
                    } else {
                        return 1;
                    }
                }

                if(t1[this.predicate] < t2[this.predicate]) {
                    if(this.reverse){
                        return 1;
                    } else {
                        return -1;
                    }                }

                return 0;
            });
        }

        // update spinner status
        // when only token search is active i.e. conditionOnlySearch = false and
        // when the search result has 0 no of trials then noTrials variable is set to true
        // which controls the view of the "Sorry! No trials" label.
        if (this.trials.length == 0 && !conditionOnlySearch) {
            this.noTrials = true;
        }else {
            this.noTrials = false;
        }
    }

     // fix for error in ng-hipster where parts are split at ',' instead it should be a location of ',<'
    private processLinks (header) {

        if (header.length === 0) {
            throw new Error('input must not be of zero length');
        }
        // Split parts by comma and < :: FIX for error in ngHipster
        var parts = header.split(',<');
        var links = {};
        // Parse each part into a named link
        parts.forEach(function (p) {
            let section = p.split(';');
            if (section.length !== 2) {
                throw new Error('section could not be split on ";"');
            }
            let url = section[0].replace(/<(.*)>/, '$1').trim();
            let queryString = <any>{};
            url.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'), function ($0, $1, $2, $3) { return queryString[$1] = $3; });
            let page = queryString.page;
            if (typeof page === 'string') {
                page = parseInt(page, 10);
            }
            let name = section[1].replace(/rel="(.*)"/, '$1').trim();
            links[name] = page;
        });
        return links;
    }

    private onImportSuccess(data) {
        this.router.navigate(['/trial/' + data.id]);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
        this.toastsManager.error('Error performing action', 'Error!');
        // update spinner status
        this.inProgress = false;
    }
}
