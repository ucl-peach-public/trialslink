import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Trial } from './trial.model';
import { InfoService } from "../../shared";

@Component({
    selector: 'search-result',
    templateUrl: './search-result.component.html',
    styleUrls: [
        'search-result.component.css'
    ]
})
export class SearchResultComponent {

    @Input() trial: Trial;
    @Input() phaseLookup: any;
    @Input() conditionsLookup: any;
    hideDescription: boolean = true;
    hideSites: boolean = true;
    singleSiteInstance: boolean;

    constructor(private infoService: InfoService
    ) {
        this.infoService.getSettings().subscribe(value => {
            this.singleSiteInstance = value.singleSiteInstance;
        });
    }

    toggleDescription() {
        this.hideDescription = !this.hideDescription;
    }

    toggleSites() {
        this.hideSites = !this.hideSites;
    }
}
