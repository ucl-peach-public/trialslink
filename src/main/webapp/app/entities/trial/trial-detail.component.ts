import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Rx';

import { JhiEventManager, JhiAlertService, JhiLanguageService  } from 'ng-jhipster';
import { Trial } from './trial.model';
import { TrialCentre } from '../trial-centre';
import { TrialService } from './trial.service';
import { TrialCentreService } from '../trial-centre/trial-centre.service';

import { InfoService, Principal } from "../../shared";
import { IdInfo } from "../id-info";

@Component({
    selector: 'jhi-trial-detail',
    templateUrl: './trial-detail.component.html'
})
export class TrialDetailComponent implements OnInit, OnDestroy {

    trial: Trial;
    newTrialCentre: TrialCentre;
    private subscription: Subscription;
    eventSubscriber: Subscription;
    private isCreating: boolean;
    @ViewChild('t') t;
    isSaving: boolean;
    isEditing: boolean = false;
    lookup: any;
    phaseLookup: any;
    private currentUserCanModifyTrial: boolean;
    inProgress: boolean = false;
    useMaterial: boolean = true;
    idInfo: IdInfo;
    singleSiteInstance: boolean;

    constructor(
        private eventManager: JhiEventManager,
        private trialService: TrialService,
        private alertService: JhiAlertService,
        private trialCentreService: TrialCentreService,
        private route: ActivatedRoute,
        private principal: Principal,
        private infoService: InfoService
    ) {
        //this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType', 'trialCentre', 'contact', 'condition', 'intervention', 'characteristic', 'sex', 'age']);
        this.lookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV'};
        this.phaseLookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV',
            zero: 'Phase 0', one: 'Phase I', two: 'Phase II', three: 'Phase III',  four: 'Phase IV'};
    }

    ngOnInit() {
        this.infoService.getSettings().subscribe(value => {
            this.singleSiteInstance = value.singleSiteInstance;
        });
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTrial();
    }

    load(id) {
        // show spinner
        this.inProgress = true;
        this.trialService.find(id).subscribe((trial) => {
            this.trial = trial;
            if (this.principal.isAuthenticated() && this.principal.hasAuthority('ROLE_ADMIN')) {
                this.currentUserCanModifyTrial = true;
            }
            if (this.principal.isAuthenticated() && this.principal.hasAuthority('ROLE_PRIVILEGED') && (this.principal.getLogin() == trial.createdBy)) {
                this.currentUserCanModifyTrial = true;
            }
            console.log('this.trial  =' , this.trial );
            // hide spinner
            this.inProgress = false;
        });
    }
    
    updateVerifyStatus()
    {
     this.trial.verified = true;
    this.trialService.update(this.trial)
            .subscribe((res: Trial) => {
                this.trial = res;
        });
    }

    requestDetails(){
        let emailContext: any;
        emailContext = this.trialService.mailContext(this.trial,'Interested%20in%20finding%20out%20more%20about%20-%20');
        this.trialService.mailto(emailContext);
    }
    
    annotate() {
        this.trialService.annotate(this.trial.id).subscribe((result) => {
            console.log("result  = " , result );
            //console.log("result body = " , result._body );
            console.log("result json  = " , result.json() );
            // reload trial
            this.load(this.trial.id);
        });
    }

    reportAnIssueMail(){
        let emailContext: any;
        emailContext = this.trialService.mailContext(this.trial,'Reporting%20an%20issue%20on:%20');
        this.trialService.mailto(emailContext);
    }

    registerChangeInTrial() {
        console.log('Recieved trial update event ');
        this.eventSubscriber = this.eventManager.subscribe('trialModification', (response) => this.load(this.trial.id));
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTrials() {
        this.eventSubscriber = this.eventManager.subscribe(
            'trialListModification',
            (response) => this.load(this.trial.id)
        );
    }
}
