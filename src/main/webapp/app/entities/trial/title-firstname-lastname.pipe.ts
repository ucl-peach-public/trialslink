import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'titleFirstnameLastname'})
export class TitleFirstnameLastnamePipe implements PipeTransform{
    transform(name: string): string {
        let nameArray = name.split(',');
        //console.log(nameArray);
        //console.log('final name: ',nameArray[1]+nameArray[0]);
        return nameArray[1]+' '+nameArray[0];
        //return name
    }

}
