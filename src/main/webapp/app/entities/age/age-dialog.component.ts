import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Age } from './age.model';
import { AgePopupService } from './age-popup.service';
import { AgeService } from './age.service';

@Component({
    selector: 'jhi-age-dialog',
    templateUrl: './age-dialog.component.html'
})
export class AgeDialogComponent implements OnInit {

    age: Age;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private ageService: AgeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.age.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ageService.update(this.age));
        } else {
            this.subscribeToSaveResponse(
                this.ageService.create(this.age));
        }
    }

    private subscribeToSaveResponse(result: Observable<Age>) {
        result.subscribe((res: Age) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Age) {
        this.eventManager.broadcast({ name: 'ageListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-age-popup',
    template: ''
})
export class AgePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private agePopupService: AgePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.agePopupService
                    .open(AgeDialogComponent as Component, params['id']);
            } else {
                this.agePopupService
                    .open(AgeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
