import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AgeComponent } from './age.component';
import { AgeDetailComponent } from './age-detail.component';
import { AgePopupComponent } from './age-dialog.component';
import { AgeDeletePopupComponent } from './age-delete-dialog.component';

export const ageRoute: Routes = [
    {
        path: 'age',
        component: AgeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.age.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'age/:id',
        component: AgeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.age.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const agePopupRoute: Routes = [
    {
        path: 'age-new',
        component: AgePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.age.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'age/:id/edit',
        component: AgePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.age.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'age/:id/delete',
        component: AgeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.age.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
