import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Age } from './age.model';
import { AgePopupService } from './age-popup.service';
import { AgeService } from './age.service';

@Component({
    selector: 'jhi-age-delete-dialog',
    templateUrl: './age-delete-dialog.component.html'
})
export class AgeDeleteDialogComponent {

    age: Age;

    constructor(
        private ageService: AgeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.ageService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ageListModification',
                content: 'Deleted an age'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-age-delete-popup',
    template: ''
})
export class AgeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private agePopupService: AgePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.agePopupService
                .open(AgeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
