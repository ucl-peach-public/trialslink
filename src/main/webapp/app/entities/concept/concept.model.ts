import { BaseEntity } from './../../shared';

export class Concept implements BaseEntity {
    constructor(
        public id?: string,
        public label?: string,
        public system?: string,
        public code?: string,
        public type?: string
    ) {
    }
}
