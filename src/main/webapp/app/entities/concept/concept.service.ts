import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import * as _ from 'underscore';
import { SERVER_API_URL } from '../../app.constants';

import { Concept } from './concept.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ConceptService {

    private resourceUrl = SERVER_API_URL + 'api/concepts';
    private statusesUrl = SERVER_API_URL + 'api/statuses';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/concepts';
    private sctSearchUrl = SERVER_API_URL + 'api/_search/sct';
    private resourceIndexUrl = SERVER_API_URL + 'api/_index/concepts';

    constructor(private http: Http) { }

    create(concept: Concept): Observable<Concept> {
        const copy = this.convert(concept);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(concept: Concept): Observable<Concept> {
        const copy = this.convert(concept);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Concept> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    allAsFilter(req?: any): Observable<Response> {
        let options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertToFilterResponse(res))
        ;
    }

    statusesAsFilter(req?: any): Observable<Response> {
        let options = createRequestOption(req);
        return this.http.get(this.statusesUrl, options)
            .map((res: any) => this.convertToFilterResponse(res))
        ;
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    indexAll(): Observable<Response> {
        return this.http.get(`${this.resourceIndexUrl}`);
    }

    search(req?: any) : Observable<ResponseWrapper> {
        let options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res))
            ;
    }

    searchSct(options: any) : Observable<any> {
        console.log("this.sctSearchUrl  = " , this.sctSearchUrl );
        let searchOpts:{ [key:string]: any; } = {};
        searchOpts.maxInstantResultsSize = 10;
        searchOpts.conceptType = ['404684003', '64572001'];
        searchOpts.maxResultsSize = 20;
        searchOpts.term = options;
        console.log("searchOpts  = " , searchOpts );

        return this.http.post(this.sctSearchUrl, searchOpts);
    }

    //private convertResponse(res: any): any {
    //    let jsonResponse = res.json();
    //    let results: Concept[] = [];
    //    for (let i = 0; i < jsonResponse.length; i++) {
    //        results.push(jsonResponse[i]);
    //    }
    //    return results;
    //}

    private convertToFilterResponse(res: any): any {
        let jsonResponse = res.json();
        let results = [];
        _.each(jsonResponse, function(item: any){
            let o = <any>{};
            o.id = item.id;
            o.text = item.label;
            o.type = item.type;
            results.push(o);
        });
        res._body = results;
        return res;
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Concept.
     */
    private convertItemFromServer(json: any): Concept {
        const entity: Concept = Object.assign(new Concept(), json);
        return entity;
    }

    /**
     * Convert a Concept to a JSON which can be sent to the server.
     */
    private convert(concept: Concept): Concept {
        const copy: Concept = Object.assign({}, concept);
        return copy;
    }
}
