import { Component, OnInit, OnDestroy, Input, ChangeDetectorRef } from '@angular/core';
import { Response } from '@angular/http';
//import {Observable} from 'rxjs/Observable';
//import 'rxjs/add/observable/of';
//import 'rxjs/add/operator/catch';
//import 'rxjs/add/operator/debounceTime';
//import 'rxjs/add/operator/distinctUntilChanged';
//import 'rxjs/add/operator/do';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/switchMap';
import { ActivatedRoute, Router } from '@angular/router';
import {Observable, Subscription, Subject} from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Condition } from './condition.model';
import { Concept } from '../concept/concept.model';
import { Trial } from '../trial/trial.model';
import { ConditionService } from './condition.service';
import { ConceptService } from '../concept/concept.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as _ from 'underscore';
import { map, distinctUntilChanged, debounceTime, switchMap, tap } from 'rxjs/operators'

@Component({
    selector: 'jhi-condition-list',
    templateUrl: './condition-list.component.html'
})
export class ConditionListComponent implements OnInit, OnDestroy {

    @Input() trial: Trial;
    conditions: Condition[];
    knownConditions: Condition[];
    knownStatuses: Array<string>;
    currentAccount: any;
    eventSubscriber: Subscription;
    addingNew: boolean;
    newCondition: Condition;
    gridLayout: boolean;
    model: any;
    searching = false;
    searchFailed = false;
    conditionToken: string;
    searchResults: Concept[];
    dropDownConditions: any[];
    isHidden= false;
    termlexConditions: any[] = [];
    conditionsLoading = false;
    conditionsTypeahead = new Subject<string>();

    constructor(
                private conditionService: ConditionService,
                private conceptService: ConceptService,
                private alertService: JhiAlertService,
                private eventManager: JhiEventManager,
                private principal: Principal,
                private cd: ChangeDetectorRef) {
        //this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType', 'trialCentre', 'contact', 'condition', 'intervention', 'characteristic', 'sex', 'age']);
        this.addingNew = false;
        this.newCondition = new Condition();
        this.gridLayout = false;
        this.knownStatuses = [];
        // add listener
        //this.registerChangeInConditions();
        this.dropDownConditions = [];

    }

    loadAll() {
        this.conditionService.conditionsForTrial(this.trial.id).subscribe(
            (res: ResponseWrapper) => {
                this.conditions = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        // load all conditions
        this.conceptService.allAsFilter({
                size: 500
            }
        ).subscribe(
            (res: Response) => {
                this.knownConditions = res.json().filter(function(c){return c.type !== 'status'; });
            },
            (res: Response) => this.onError(res.json())
        );
        // load all statuses
        this.conceptService.statusesAsFilter({
                size: 20
            }
        ).subscribe(
            (res: Response) => {
                this.knownStatuses = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConditions();
        this.loadConditions();
    }

    onConditionItemAdded($event: any): void {
        console.log("newCondition", this.newCondition);
        console.log("$event  = " , $event );
        this.newCondition.label = $event.label;
        this.newCondition.code = $event.id;
    }

    requestAutocompleteItems = (text: string): Observable<Response> => {

        console.log("text  = " , text );
        let searchOpts:{ [key:string]: any; } = {};
        searchOpts.maxInstantResultsSize = 10;
        searchOpts.conceptType = ['404684003', '64572001'];
        searchOpts.maxResultsSize = 10;
        searchOpts.term = text;
        console.log("searchOpts  = " , searchOpts );
        return this.conceptService.searchSct(searchOpts)
            .pipe(map(data => data.json().results
                .map((resultItem: any) => {
                    let item = { display: resultItem.label, value: resultItem.id };
                    return item;
                })));
    };

     private loadConditions() {
            this.conditionsTypeahead.pipe(
                tap(() => this.conditionsLoading = true),
                distinctUntilChanged(),
                debounceTime(200),
                switchMap(term => this.conceptService.searchSct(term)),
            ).subscribe(x => {
                    console.log("x  = " , x );
                    console.log("x.json()  = " , x.json() );
                    this.termlexConditions = x.json().results;
                    this.conditionsLoading = false;
                    this.cd.markForCheck();
                }, () => {
                    this.termlexConditions = [];
                });
     }


    addCondition() {
        this.newCondition = new Condition();
        this.newCondition.trialId = this.trial.id;
        this.addingNew = true;
    }

    save() {
        this.conditionService.create(this.newCondition)
            .subscribe((res: Condition) =>
                this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
    }

    deleteAllConditions() {
        this.conditionService.deleteAllForTrial(this.trial.id)
            .subscribe((res: any) =>
                this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
    }

    /*onConditionSelect(item: any) {
        this.newCondition.label = item.text;
        this.newCondition.code = item.id;
        // this.newCondition.system = item.system;
    }*/

    onStatusSelect(item: any) {
        this.newCondition.status = item.text;
        this.newCondition.statusCode = item.id;
    }

    private onSaveSuccess(result: Condition) {
        this.eventManager.broadcast({name: 'conditionListModification', content: 'OK'});
        this.eventManager.broadcast({name: 'trialModification', content: 'OK'});
        this.addingNew = false;
    }

    private onSaveError(error) {
        this.addingNew = false;
        this.onError(error);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Condition) {
        return item.id;
    }

    registerChangeInConditions() {
        this.eventSubscriber = this.eventManager.subscribe('conditionListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
