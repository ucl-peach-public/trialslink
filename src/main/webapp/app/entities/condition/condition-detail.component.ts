import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Response } from '@angular/http';

import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService, JhiLanguageService} from 'ng-jhipster';
import { Observable, Subscription } from 'rxjs/Rx';
import { Condition } from './condition.model';
import { ConditionService } from './condition.service';
import { ConceptService } from '../concept/concept.service';
import {map} from "rxjs/operators";

@Component({
    selector: 'jhi-condition-detail',
    templateUrl: './condition-detail.component.html'
})
export class ConditionDetailComponent implements OnInit, OnDestroy {

    @Input() condition: Condition;
    @Input() gridLayout: boolean;
    @Input() knownConditions: Condition[];
    @Input() knownStatuses: Condition[];
    private editing: boolean;
    private eventSubscriber: Subscription;
    authorities: any[];
    isSaving: boolean;
    dropDownConditions: any[];

    constructor(
                private conceptService: ConceptService,
                private conditionService: ConditionService,
                private alertService: JhiAlertService,
                private eventManager: JhiEventManager,
                //private subscription: Subscription,
                //private eventSubscriber: Subscription,
                private route: ActivatedRoute) {
        //this.jhiLanguageService.setLocations(['condition', 'trialCentre', 'trial']);
        this.editing = false;
        this.dropDownConditions = [];
    }

    ngOnInit() {
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.registerChangeInConditions();
        // this.subscription = this.route.params.subscribe(params => {
        //    this.load(params['id']);
        // });
    }
    
    onConditionItemAdded($event: any): void {
        console.log("$event  = " , $event );
        this.condition.label = $event.display;
        this.condition.code = $event.value;
    }

    requestAutocompleteItems = (text: string): Observable<Response> => {

        console.log("text  = " , text );
        let searchOpts:{ [key:string]: any; } = {};
        searchOpts.maxInstantResultsSize = 10;
        searchOpts.conceptType = ['404684003', '64572001'];
        searchOpts.maxResultsSize = 10;
        searchOpts.term = text;
        console.log("searchOpts  = " , searchOpts );
        return this.conceptService.searchSct(searchOpts)
            .pipe(map(data => data.json().results
                .map((resultItem: any) => {
                    let item = { display: resultItem.label, value: resultItem.id };
                    return item;
                })));
    };

    load(id) {
        this.conditionService.find(id).subscribe((condition) => {
            this.condition = condition;
        });
    }

    /*onConditionSelect(item: any) {
        this.condition.label = item.text;
        this.condition.code = item.id;
        // this.condition.system = item.system;
    }*/

    onStatusSelect(item: any) {
        this.condition.status = item.text;
        this.condition.statusCode = item.id;
    }

    update() {
        this.isSaving = true;
        this.conditionService.update(this.condition)
            .subscribe((res: Condition) =>
                this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
    }

    private onSaveSuccess(result: Condition) {
        this.eventManager.broadcast({name: 'conditionListModification', content: 'OK'});
        this.isSaving = false;
        this.editing = false;
    }

    private onSaveError(error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        //if (this.subscription) {
        //    this.subscription.unsubscribe();
        //}
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConditions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'conditionListModification',
            (response) => this.load(this.condition.id)
        );
    }
}
