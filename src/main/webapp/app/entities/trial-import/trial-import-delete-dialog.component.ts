import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TrialImport } from './trial-import.model';
import { TrialImportPopupService } from './trial-import-popup.service';
import { TrialImportService } from './trial-import.service';

@Component({
    selector: 'jhi-trial-import-delete-dialog',
    templateUrl: './trial-import-delete-dialog.component.html'
})
export class TrialImportDeleteDialogComponent {

    trialImport: TrialImport;

    constructor(
        private trialImportService: TrialImportService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.trialImportService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'trialImportListModification',
                content: 'Deleted an trialImport'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trial-import-delete-popup',
    template: ''
})
export class TrialImportDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private trialImportPopupService: TrialImportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.trialImportPopupService
                .open(TrialImportDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
