import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicService } from './characteristic.service';

@Component({
    selector: 'jhi-characteristic-detail',
    templateUrl: './characteristic-detail.component.html'
})
export class CharacteristicDetailComponent implements OnInit, OnDestroy {

    characteristic: Characteristic;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private characteristicService: CharacteristicService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCharacteristics();
    }

    load(id) {
        this.characteristicService.find(id).subscribe((characteristic) => {
            this.characteristic = characteristic;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCharacteristics() {
        this.eventSubscriber = this.eventManager.subscribe(
            'characteristicListModification',
            (response) => this.load(this.characteristic.id)
        );
    }
}
