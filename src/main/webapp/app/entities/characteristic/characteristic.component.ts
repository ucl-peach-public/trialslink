import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicService } from './characteristic.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-characteristic',
    templateUrl: './characteristic.component.html'
})
export class CharacteristicComponent implements OnInit, OnDestroy {
characteristics: Characteristic[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private characteristicService: CharacteristicService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.characteristicService.query().subscribe(
            (res: ResponseWrapper) => {
                this.characteristics = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCharacteristics();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Characteristic) {
        return item.id;
    }
    registerChangeInCharacteristics() {
        this.eventSubscriber = this.eventManager.subscribe('characteristicListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
