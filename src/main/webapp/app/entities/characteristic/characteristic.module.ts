import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    CharacteristicService,
    CharacteristicPopupService,
    CharacteristicComponent,
    CharacteristicListComponent,
    CharacteristicDetailComponent,
    CharacteristicDialogComponent,
    CharacteristicPopupComponent,
    CharacteristicDeletePopupComponent,
    CharacteristicDeleteDialogComponent,
    characteristicRoute,
    characteristicPopupRoute,
} from './';

const ENTITY_STATES = [
    ...characteristicRoute,
    ...characteristicPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CharacteristicComponent,
        CharacteristicListComponent,
        CharacteristicDetailComponent,
        CharacteristicDialogComponent,
        CharacteristicDeleteDialogComponent,
        CharacteristicPopupComponent,
        CharacteristicDeletePopupComponent,
    ],
    entryComponents: [
        CharacteristicComponent,
        CharacteristicListComponent,
        CharacteristicDialogComponent,
        CharacteristicPopupComponent,
        CharacteristicDeleteDialogComponent,
        CharacteristicDeletePopupComponent,
    ],
    providers: [
        CharacteristicService,
        CharacteristicPopupService,
    ],
    exports: [
        CharacteristicListComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkCharacteristicModule {}
