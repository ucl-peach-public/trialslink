import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Characteristic } from './characteristic.model';
import { CharacteristicService } from './characteristic.service';

@Injectable()
export class CharacteristicPopupService {

    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private characteristicService: CharacteristicService

    ) {
        this.ngbModalRef = null;
    }

    //open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
    //    if (this.isOpen) {
    //        return;
    //    }
    //    this.isOpen = true;
    //
    //    if (!createNew) {
    //        this.characteristicService.find(id).subscribe(characteristic => {
    //            this.characteristicModalRef(component, characteristic);
    //        });
    //    } else {
    //        let characteristic = new Characteristic();
    //        characteristic.trialId = id;
    //        return this.characteristicModalRef(component, characteristic);
    //    }
    open(component: Component, id?: number | any, createNew?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (!createNew) {
                this.characteristicService.find(id).subscribe((characteristic) => {
                    this.ngbModalRef = this.characteristicModalRef(component, characteristic);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.characteristicModalRef(component, new Characteristic());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    characteristicModalRef(component: Component, characteristic: Characteristic): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.characteristic = characteristic;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
