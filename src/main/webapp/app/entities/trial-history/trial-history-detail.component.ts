import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {JhiEventManager, JhiLanguageService} from 'ng-jhipster';

import { TrialHistory } from './trial-history.model';
import { TrialHistoryService } from './trial-history.service';

import { diff } from 'deep-diff';
//import { objectPath } from 'object-path';
import * as objectPath from 'object-path';

@Component({
    selector: 'jhi-trial-history-detail',
    templateUrl: './trial-history-detail.component.html'
})
export class TrialHistoryDetailComponent implements OnInit, OnDestroy {

    trialHistory: TrialHistory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    private snapshotIds: string[];
    private earliestSnapshotId: string;
    private mostRecentSnapshotId: string;
    private isShowingDiffView: boolean;
    private diffOutput: any;
    private diffRow: number;

    constructor(
        private eventManager: JhiEventManager,
        //private jhiLanguageService: JhiLanguageService,
        private trialHistoryService: TrialHistoryService,
        private route: ActivatedRoute
    ) {
        //this.jhiLanguageService.setLocations(['trialHistory']);
        this.isShowingDiffView = false;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTrialHistories();
    }

    load(id) {
        this.trialHistoryService.find(id).subscribe((trialHistory) => {
            this.trialHistory = trialHistory;

            this.snapshotIds = [];
            this.earliestSnapshotId = '';
            this.mostRecentSnapshotId = '';

            for (let snapshotId in this.trialHistory.snapshots) {
                if (this.earliestSnapshotId == '' || Number(snapshotId) < Number(this.earliestSnapshotId)) {
                    this.earliestSnapshotId = snapshotId;
                }
                if (this.mostRecentSnapshotId == '' || Number(snapshotId) > Number(this.mostRecentSnapshotId)) {
                    this.mostRecentSnapshotId = snapshotId;
                }
                this.snapshotIds.push(snapshotId);
            }

            // sort snapshotIds in "descending" order so that the most recent snapshot is at index "0"
            this.snapshotIds = this.snapshotIds.sort((a, b) => Number(b) - Number(a));
        });
    }

    diffSnapshots(oldSnapshotId, newSnapshotId, diffRow) {
        this.diffRow = diffRow;
        this.isShowingDiffView = true;

        this.diffOutput = diff(this.trialHistory.snapshots[oldSnapshotId], this.trialHistory.snapshots[newSnapshotId]);

        for (let diffObject of this.diffOutput) {
            diffObject["patch"] = this.buildPatch(diffObject);
        }
    }

    buildPatch(diffObject) {
        let patchObject = {
            lhs: {},
            rhs: {}
        };

        if (diffObject.kind == "D") {
            patchObject = this.getDeletePatchObject(diffObject, diffObject.path);
        }
        if (diffObject.kind == "E") {
            patchObject = this.getEditPatchObject(diffObject, diffObject.path);
        }
        if (diffObject.kind == "N") {
            patchObject = this.getNewPatchObject(diffObject, diffObject.path);
        }
        if (diffObject.kind == "A") {
            let path = diffObject.path;
            path.push(diffObject.index);

            switch (diffObject.item.kind) {
                case "D": {
                    patchObject = this.getDeletePatchObject(diffObject.item, path);
                    break;
                }

                case "E": {
                    patchObject = this.getEditPatchObject(diffObject.item, path);
                    break;
                }

                case "N": {
                    patchObject = this.getNewPatchObject(diffObject.item, path);
                    break;
                }
            }
        }

        return patchObject;
    }

    getEditPatchObject(diffItem, path) {
        let patchObject = {
            lhs: {},
            rhs: {}
        };
        patchObject.lhs = TrialHistoryDetailComponent.buildPatchTemplate(path);
        patchObject.rhs = TrialHistoryDetailComponent.buildPatchTemplate(path);

        objectPath.set(patchObject.lhs, path, diffItem.lhs);
        objectPath.set(patchObject.rhs, path, diffItem.rhs);

        return patchObject;
    }

    getNewPatchObject(diffItem, path) {
        let patchObject = {
            lhs: {},
            rhs: {}
        };

        patchObject.rhs = TrialHistoryDetailComponent.buildPatchTemplate(path);
        objectPath.set(patchObject.rhs, path, diffItem.rhs);

        return patchObject;
    }

    getDeletePatchObject(diffItem, path) {
        let patchObject = {
            lhs: {},
            rhs: {}
        };

        patchObject.lhs = TrialHistoryDetailComponent.buildPatchTemplate(path);
        objectPath.set(patchObject.lhs, path, diffItem.lhs);

        return patchObject;
    }

    static buildPatchTemplate(path) {
        let patchObject: any;
        let currentPatchObject: any;
        let pathIndex:number = path.length - 1;
        /*if (typeof path[pathIndex] == 'string') {
            patchObject = {};
            patchObject[path[pathIndex]] = undefined;
        } else {
            patchObject = [];
            patchObject[path[pathIndex]] = undefined;
        }
        pathIndex--;*/
        while (pathIndex >= 0) {
            currentPatchObject = patchObject;
            if (typeof path[pathIndex] == 'number') {
                patchObject = [];
                patchObject[path[pathIndex]] = currentPatchObject;
            } else {
                patchObject = {};
                patchObject[path[pathIndex]] = currentPatchObject;
            }
            pathIndex--;
        }
        return patchObject;
    }

    closeDiff() {
        this.isShowingDiffView = false;
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTrialHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'trialHistoryListModification',
            (response) => this.load(this.trialHistory.id)
        );
    }
}
