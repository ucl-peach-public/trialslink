import {Trial} from "../trial/trial.model";
import { BaseEntity } from './../../shared';

export class TrialHistory implements BaseEntity {
    constructor(
        public id?: string,
        public snapshots?: {key: string, value: Trial}[],
    ) {
    }
}
