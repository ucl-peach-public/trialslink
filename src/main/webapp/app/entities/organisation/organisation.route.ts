import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { OrganisationComponent } from './organisation.component';
import { OrganisationDetailComponent } from './organisation-detail.component';
import { OrganisationPopupComponent } from './organisation-dialog.component';
import { OrganisationDeletePopupComponent } from './organisation-delete-dialog.component';

export const organisationRoute: Routes = [
    {
        path: 'organisation',
        component: OrganisationComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.organisation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'organisation/:id',
        component: OrganisationDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.organisation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const organisationPopupRoute: Routes = [
    {
        path: 'organisation-new',
        component: OrganisationPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.organisation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'organisation/:id/edit',
        component: OrganisationPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.organisation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'organisation/:id/delete',
        component: OrganisationDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.organisation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
