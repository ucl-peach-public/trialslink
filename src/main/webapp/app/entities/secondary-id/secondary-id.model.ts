import { BaseEntity } from './../../shared';

export class SecondaryId implements BaseEntity {
    constructor(
        public id?: string,
        public type?: string,
        public value?: string,
    ) {
    }
}
