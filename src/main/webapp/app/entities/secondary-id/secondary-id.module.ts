import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    SecondaryIdService,
    SecondaryIdPopupService,
    SecondaryIdComponent,
    SecondaryIdDetailComponent,
    SecondaryIdDialogComponent,
    SecondaryIdPopupComponent,
    SecondaryIdDeletePopupComponent,
    SecondaryIdDeleteDialogComponent,
    secondaryIdRoute,
    secondaryIdPopupRoute,
} from './';

const ENTITY_STATES = [
    ...secondaryIdRoute,
    ...secondaryIdPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SecondaryIdComponent,
        SecondaryIdDetailComponent,
        SecondaryIdDialogComponent,
        SecondaryIdDeleteDialogComponent,
        SecondaryIdPopupComponent,
        SecondaryIdDeletePopupComponent,
    ],
    entryComponents: [
        SecondaryIdComponent,
        SecondaryIdDialogComponent,
        SecondaryIdPopupComponent,
        SecondaryIdDeleteDialogComponent,
        SecondaryIdDeletePopupComponent,
    ],
    providers: [
        SecondaryIdService,
        SecondaryIdPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkSecondaryIdModule {}
