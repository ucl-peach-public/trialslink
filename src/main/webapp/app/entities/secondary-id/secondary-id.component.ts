import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdService } from './secondary-id.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-secondary-id',
    templateUrl: './secondary-id.component.html'
})
export class SecondaryIdComponent implements OnInit, OnDestroy {
secondaryIds: SecondaryId[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private secondaryIdService: SecondaryIdService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.secondaryIdService.query().subscribe(
            (res: ResponseWrapper) => {
                this.secondaryIds = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSecondaryIds();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SecondaryId) {
        return item.id;
    }
    registerChangeInSecondaryIds() {
        this.eventSubscriber = this.eventManager.subscribe('secondaryIdListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
