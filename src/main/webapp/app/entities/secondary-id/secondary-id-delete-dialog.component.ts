import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdPopupService } from './secondary-id-popup.service';
import { SecondaryIdService } from './secondary-id.service';

@Component({
    selector: 'jhi-secondary-id-delete-dialog',
    templateUrl: './secondary-id-delete-dialog.component.html'
})
export class SecondaryIdDeleteDialogComponent {

    secondaryId: SecondaryId;

    constructor(
        private secondaryIdService: SecondaryIdService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.secondaryIdService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'secondaryIdListModification',
                content: 'Deleted an secondaryId'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-secondary-id-delete-popup',
    template: ''
})
export class SecondaryIdDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private secondaryIdPopupService: SecondaryIdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.secondaryIdPopupService
                .open(SecondaryIdDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
