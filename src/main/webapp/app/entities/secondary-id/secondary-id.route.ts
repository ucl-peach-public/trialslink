import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SecondaryIdComponent } from './secondary-id.component';
import { SecondaryIdDetailComponent } from './secondary-id-detail.component';
import { SecondaryIdPopupComponent } from './secondary-id-dialog.component';
import { SecondaryIdDeletePopupComponent } from './secondary-id-delete-dialog.component';

export const secondaryIdRoute: Routes = [
    {
        path: 'secondary-id',
        component: SecondaryIdComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.secondaryId.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'secondary-id/:id',
        component: SecondaryIdDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.secondaryId.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const secondaryIdPopupRoute: Routes = [
    {
        path: 'secondary-id-new',
        component: SecondaryIdPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.secondaryId.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'secondary-id/:id/edit',
        component: SecondaryIdPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.secondaryId.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'secondary-id/:id/delete',
        component: SecondaryIdDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.secondaryId.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
