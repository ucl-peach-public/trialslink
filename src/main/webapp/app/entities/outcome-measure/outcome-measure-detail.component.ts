import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { OutcomeMeasure } from './outcome-measure.model';
import { OutcomeMeasureService } from './outcome-measure.service';

@Component({
    selector: 'jhi-outcome-measure-detail',
    templateUrl: './outcome-measure-detail.component.html'
})
export class OutcomeMeasureDetailComponent implements OnInit, OnDestroy {

    outcomeMeasure: OutcomeMeasure;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private outcomeMeasureService: OutcomeMeasureService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOutcomeMeasures();
    }

    load(id) {
        this.outcomeMeasureService.find(id).subscribe((outcomeMeasure) => {
            this.outcomeMeasure = outcomeMeasure;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOutcomeMeasures() {
        this.eventSubscriber = this.eventManager.subscribe(
            'outcomeMeasureListModification',
            (response) => this.load(this.outcomeMeasure.id)
        );
    }
}
