import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Practitioner } from './practitioner.model';
import { PractitionerService } from './practitioner.service';

@Component({
    selector: 'jhi-practitioner-detail',
    templateUrl: './practitioner-detail.component.html'
})
export class PractitionerDetailComponent implements OnInit, OnDestroy {

    practitioner: Practitioner;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private practitionerService: PractitionerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPractitioners();
    }

    load(id) {
        this.practitionerService.find(id).subscribe((practitioner) => {
            this.practitioner = practitioner;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPractitioners() {
        this.eventSubscriber = this.eventManager.subscribe(
            'practitionerListModification',
            (response) => this.load(this.practitioner.id)
        );
    }
}
