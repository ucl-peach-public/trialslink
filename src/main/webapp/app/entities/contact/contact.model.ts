import { Role } from '../role';
import { BaseEntity } from './../../shared';

export class Contact implements BaseEntity {
    constructor(
        public id?: string,
        public phone?: string,
        public personName?: string,
        public email?: string,
        public role?: Role,
        public trialCentreId?: string
    ) {
        this.role = new Role();
    }
}
