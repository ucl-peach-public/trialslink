import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Contact } from './contact.model';
import { ContactService } from './contact.service';

@Injectable()
export class ContactPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private contactService: ContactService

    ) {
        this.ngbModalRef = null;
    }

    //open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (!createNew) {
//            this.contactService.find(id).subscribe(contact => {
//                this.contactModalRef(component, contact);
//            });
//        } else {
//            let contact = new Contact();
//            contact.trialCentreId = id;
//            return this.contactModalRef(component, contact);
//        }
//=======
    open(component: Component, id?: number | any, createNew?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (!createNew) {
                this.contactService.find(id).subscribe((contact) => {
                    this.ngbModalRef = this.contactModalRef(component, contact);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    let contact = new Contact();
                    contact.trialCentreId = id;
                    this.ngbModalRef = this.contactModalRef(component, contact);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    contactModalRef(component: Component, contact: Contact): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.contact = contact;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
