import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Intervention } from './intervention.model';
import { InterventionService } from './intervention.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-intervention',
    templateUrl: './intervention.component.html'
})
export class InterventionComponent implements OnInit, OnDestroy {
interventions: Intervention[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private interventionService: InterventionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.interventionService.query().subscribe(
            (res: ResponseWrapper) => {
                this.interventions = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInInterventions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Intervention) {
        return item.id;
    }
    registerChangeInInterventions() {
        this.eventSubscriber = this.eventManager.subscribe('interventionListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
