import { Component, OnInit } from '@angular/core';
import {JhiAlertService, JhiLanguageService} from 'ng-jhipster';

import { Principal, AccountService, JhiLanguageHelper } from '../../shared';
import {PrivilegeRequestService} from "../../entities/privilege-request/privilege-request.service";
import {PrivilegeRequest, PrivilegeRequestStatus} from "../../entities/privilege-request/privilege-request.model";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
    error: string;
    success: string;
    settingsAccount: any;
    languages: any[];
    isPrivilegedUser: boolean;

    constructor(
        private account: AccountService,
        private privilegeRequestService: PrivilegeRequestService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper
    ) {
    }

//<<<<<<< HEAD
    ngOnInit () {
        this.loadSettingsAccount();
        this.principal.hasAnyAuthority(["ROLE_PRIVILEGED", "ROLE_ADMIN"]).then(result => {
            this.isPrivilegedUser = result;
//=======
//    ngOnInit() {
//        this.principal.identity().then((account) => {
//            this.settingsAccount = this.copyAccount(account);
//>>>>>>> jhipster_upgrade
        });
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });
    }

    save() {
        this.account.save(this.settingsAccount).subscribe(() => {
            this.error = null;
            this.success = 'OK';
            this.loadSettingsAccount(true);
            this.languageService.getCurrent().then((current) => {
                if (current!= 'uclh') {
                    if (this.settingsAccount.langKey !== current) {
                        this.languageService.changeLanguage(this.settingsAccount.langKey);
                    }
                }
            });
        }, () => {
            this.success = null;
            this.error = 'ERROR';
        });
    }

    requestPrivilegedAccess() {
        console.log("requesting privileged access...");
        let privilegeRequest:PrivilegeRequest = new PrivilegeRequest();
        privilegeRequest.status = PrivilegeRequestStatus.PENDING;

        this.privilegeRequestService.create(privilegeRequest).subscribe((res: PrivilegeRequest) => {
            this.alertService.success("Successfully requested edit access!");
            this.loadSettingsAccount(true);
        }, (error: any) => {
            this.alertService.error("Error requesting edit access!");
        });
    }

    loadSettingsAccount(forceRefresh?: boolean) {
        this.principal.identity(forceRefresh).then((account) => {
            this.settingsAccount = this.copyAccount(account);
        });
    }

    copyAccount (account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            pendingPrivilegeRequest: account.pendingPrivilegeRequest
        };
    }
}
