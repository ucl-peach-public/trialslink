import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {LocalStorageService} from "ngx-webstorage";


@Injectable()
export class InfoService  {
    constructor(private http: Http, private localStorageService: LocalStorageService) { }

    get(): Observable<any> {
        return this.http.get('api/info').map((res: Response) => res.json());
    }

    getBrand(): Observable<any> {
        return this.http.get('api/brand').map((res: Response) => res);
    }

    preferOmnisearch(): Observable<any> {
        return this.http.get('api/omnisearch').map((res: Response) => res.json());
    }

    allowRegistrations(): Observable<any>{
        return this.http.get('api/registrations').map((res: Response) => res.json());
    }

    showLanguageSwitcher(): Observable<any>{
        return this.http.get('api/languageswitcher').map((res: Response) => res.json());
    }

    getRibbonInfo(): Observable<any> {
        return this.http.get('api/ribbon').map((res: Response) => res.json());
    }

    getSettings(): Observable<any> {
        return this.http.get('api/settings').map((res: Response) => res.json());
    }

    useLocalStorage(): Observable<any>{
        return this.http.get('api/localstorage').map((res: Response) => {
            if (!res.json()){
                this.localStorageService.clear();
            }
            return res.json();
        });
    }
}
