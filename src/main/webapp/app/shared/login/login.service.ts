import { Injectable } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';

import { Principal } from '../auth/principal.service';
import { AuthServerProvider } from '../auth/auth-jwt.service';
import {Observable, Subject} from "rxjs";
import {InfoService} from "..";

@Injectable()
export class LoginService {

    private log = new Subject<Boolean>();
    public log$ = this.log.asObservable();
    brand: string

    constructor(
        private languageService: JhiLanguageService,
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private infoService: InfoService
    ) {
        this.infoService.getBrand().subscribe(brand => {
            this.brand = brand._body;
        });
    }

    login(credentials, callback?) {
        const cb = callback || function() {};

        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe((data) => {
                this.principal.identity(true).then((account) => {
                    // After the login the language will be changed to
                    // the language selected by the user during his registration
                    if (account !== null) {
                        if (this.brand!='uclh') {
                            this.languageService.changeLanguage(account.langKey);
                        }
                    }
                    resolve(data);
                });
                this.logStatus();
                return cb();
            }, (err) => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }

    //method for interacting with another component about login status
    logStatus(): Observable<any>{
        this.log.next();
        return this.log;
    }

    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
        this.logStatus();
    }
}
