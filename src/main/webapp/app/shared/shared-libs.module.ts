import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgJhipsterModule } from 'ng-jhipster';
import {SelectModule} from 'ng2-select-compat';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // needed for Angular 2.x version of TagInputModule
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CookieModule } from 'ngx-cookie';
import { TruncateModule } from 'ng2-truncate';
import {UiSwitchModule} from "ngx-ui-switch";
import { LoadingModule } from 'ngx-loading';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TermlexModule } from 'termlex-ng';
import { NgSelectModule } from '@ng-select/ng-select';
import {NgcCookieConsentModule, NgcCookieConsentConfig} from 'ngx-cookieconsent';

const cookieConfig:NgcCookieConsentConfig = {
    cookie: {
        domain: 'localhost' // or 'your.domain.com' // it is mandatory to set a domain, for cookies to work properly (see https://goo.gl/S2Hy2A)
    },
    palette: {
        popup: {
            background: '#e0f3f0',
            text: '#666',
            link: '#00727C'
        },
        button: {
            background: '#00727C'
        }
    },
    position: 'top',
    theme: 'classic',
    type: 'info',
    content: {
        href: 'https://www.uclh.nhs.uk/aboutus/Pages/Cookiepolicy.aspx',
    }
};

@NgModule({
    imports: [
        HttpModule,
        HttpClientModule,
        NgbModule.forRoot(),
        NgJhipsterModule.forRoot({
            // set below to true to make alerts look like toast
            alertAsToast: false,
            i18nEnabled: true,
            defaultI18nLang: 'en'
        }),
        TermlexModule.forRoot({
            token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1NzczMjQzOX0.4THAACWt7xHpbVofLD3ILaLo1bIeSAR_4lWq5AGbYBRDI1lvGeU-CxRTx8Okn7-I8DY9g5k6rKAnZB6OPVBkYQ',
            url: 'http://localhost:8221',
            resultOpts: {
                maxInstantResultsSize: 100,  // max number of instant results
                conceptType: "919191919191441993", // conceptId of SNOMED CT top level concept - returns concepts in all hierarchies
                maxResultsSize: 100
            }
        }),
        NgcCookieConsentModule.forRoot(cookieConfig),
        InfiniteScrollModule,
        NgSelectModule,
        //SelectModule,
        TagInputModule,
        BrowserAnimationsModule,
        TruncateModule,
        UiSwitchModule,
        LoadingModule,
        NoopAnimationsModule,
        NgxChartsModule,
        ToastModule.forRoot(),
        CookieModule.forRoot()
    ],
    exports: [
        FormsModule,
        HttpModule,
        HttpClientModule,
        CommonModule,
        NgbModule,
        NgJhipsterModule,
        InfiniteScrollModule,
        TermlexModule,
        NgSelectModule,
        //SelectModule,
        TagInputModule,
        BrowserAnimationsModule,
        TruncateModule,
        UiSwitchModule,
        LoadingModule,
        NoopAnimationsModule,
        NgxChartsModule,
        ToastModule
    ]
})
export class TrialslinkSharedLibsModule {}
