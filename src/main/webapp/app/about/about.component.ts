import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiLanguageService} from 'ng-jhipster';
import * as _ from 'underscore';
import {LocalStorageService, LocalStorage} from 'ngx-webstorage';
import { ConceptService } from '../entities/concept/concept.service';
import { TrialService } from '../entities/trial/trial.service';

import {Account, InfoService, LoginModalService, Principal} from '../shared';

@Component({
    selector: 'jhi-about',
    templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    brand: string;
    //useLocalStorage: boolean;

    constructor(
        //private principal: Principal,
        //private loginModalService: LoginModalService,
        //private eventManager: JhiEventManager,
        //private trialService: TrialService,
        //private conceptService: ConceptService,
        //private localStorageService:LocalStorageService,
        //private router: Router,
        //private languageService: JhiLanguageService,
        //private infoService: InfoService
    ) {
    }

    ngOnInit() {
        //this.principal.identity().then((account) => {
        //    this.account = account;
        //});
        //this.registerAuthenticationSuccess();
        //this.infoService.getBrand().subscribe(brand => {
        //    this.brand = brand._body;
        //    if (this.brand==='uclh') {
        //        this.languageService.changeLanguage('uclh');
        //    }
        //});
        //this.infoService.useLocalStorage().subscribe(value => {
        //    this.useLocalStorage = value;
        //});
    }

    //registerAuthenticationSuccess() {
    //    this.eventManager.subscribe('authenticationSuccess', (message) => {
    //        this.principal.identity().then((account) => {
    //            this.account = account;
    //        });
    //    });
    //}
}
