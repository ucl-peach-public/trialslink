import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { ProfileService } from '../profiles/profile.service';
import {JhiLanguageHelper, Principal, LoginModalService, LoginService, InfoService} from '../../shared';

import { VERSION } from '../../app.constants';
import {Register, RegisterComponent} from "../../account";
import {LocalStorageService} from "ngx-webstorage";

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        'navbar.css'
    ]
})
export class NavbarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    isShowSidebarButton: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    brand: string;
    allowRegistrations: boolean;
    showLanguageSwitcher: boolean;
    useLocalStorage: boolean;

    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router,
        private eventManager: JhiEventManager,
        private infoService: InfoService,
        private localStorageService: LocalStorageService
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        this.isShowSidebarButton = false;
        this.infoService.allowRegistrations().subscribe(value => {
            this.allowRegistrations = value;
        });
        this.infoService.showLanguageSwitcher().subscribe(value => {
            this.showLanguageSwitcher = value;
            this.getLanguages();
        });
        this.infoService.useLocalStorage().subscribe(value => {
            this.useLocalStorage = value;
        });
        this.infoService.getBrand().subscribe( brand => {
            this.brand = brand._body;
            if (this.brand == 'uclh') {
                this.title('favicon_uclh.ico');
            }
            else {
                this.title('favicon.ico')
            }
        });

    }

    ngOnInit() {

        this.profileService.getProfileInfo().then((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
    }

    title(icon){
        var linkElement = document.createElement( "link" );
        linkElement.setAttribute( "rel", "shortcut icon" );
        linkElement.setAttribute( "type", 'image/ico' );
        linkElement.setAttribute( "href", icon );
        document.head.appendChild( linkElement );
    }

    getLanguages() {
        if (this.showLanguageSwitcher) {
            this.languageHelper.getAll().then((languages) => {
                this.languages = languages;
            });
        }
    }

    changeLanguage(languageKey: string) {
        if (this.brand.match('uclh') || this.brand === 'uclh'){
            this.languageService.changeLanguage('uclh');
        }
        else {
            this.languageService.changeLanguage(languageKey);
        }
    }

    clearLocalStorage() {
        this.localStorageService.clear();
    }

    toggleSideBar() {
        this.eventManager.broadcast({
            name: 'sideBarToggled',
            content: 'Toggled side bar'
        });
        console.log('Toggled side bar');
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }
}
