package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.IdInfo;

import com.trialslink.repository.IdInfoRepository;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing IdInfo.
 */
@RestController
@RequestMapping("/api")
public class IdInfoResource {

    private final Logger log = LoggerFactory.getLogger(IdInfoResource.class);

    private static final String ENTITY_NAME = "idInfo";

    private final IdInfoRepository idInfoRepository;

    public IdInfoResource(IdInfoRepository idInfoRepository) {
        this.idInfoRepository = idInfoRepository;
    }

    /**
     * POST  /id-infos : Create a new idInfo.
     *
     * @param idInfo the idInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new idInfo, or with status 400 (Bad Request) if the idInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/id-infos")
    @Timed
    public ResponseEntity<IdInfo> createIdInfo(@Valid @RequestBody IdInfo idInfo) throws URISyntaxException {
        log.debug("REST request to save IdInfo : {}", idInfo);
        if (idInfo.getId() != null) {
            throw new BadRequestAlertException("A new idInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IdInfo result = idInfoRepository.save(idInfo);
        return ResponseEntity.created(new URI("/api/id-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /id-infos : Updates an existing idInfo.
     *
     * @param idInfo the idInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated idInfo,
     * or with status 400 (Bad Request) if the idInfo is not valid,
     * or with status 500 (Internal Server Error) if the idInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/id-infos")
    @Timed
    public ResponseEntity<IdInfo> updateIdInfo(@Valid @RequestBody IdInfo idInfo) throws URISyntaxException {
        log.debug("REST request to update IdInfo : {}", idInfo);
        if (idInfo.getId() == null) {
            return createIdInfo(idInfo);
        }
        IdInfo result = idInfoRepository.save(idInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, idInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /id-infos : get all the idInfos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of idInfos in body
     */
    @GetMapping("/id-infos")
    @Timed
    public List<IdInfo> getAllIdInfos() {
        log.debug("REST request to get all IdInfos");
        return idInfoRepository.findAll();
        }

    /**
     * GET  /id-infos/:id : get the "id" idInfo.
     *
     * @param id the id of the idInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the idInfo, or with status 404 (Not Found)
     */
    @GetMapping("/id-infos/{id}")
    @Timed
    public ResponseEntity<IdInfo> getIdInfo(@PathVariable String id) {
        log.debug("REST request to get IdInfo : {}", id);
        IdInfo idInfo = idInfoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(idInfo));
    }

    /**
     * DELETE  /id-infos/:id : delete the "id" idInfo.
     *
     * @param id the id of the idInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/id-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteIdInfo(@PathVariable String id) {
        log.debug("REST request to delete IdInfo : {}", id);
        idInfoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
