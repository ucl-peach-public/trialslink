package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Intervention;

import com.trialslink.domain.Trial;
import com.trialslink.repository.InterventionRepository;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Intervention.
 */
@RestController
@RequestMapping("/api")
public class InterventionResource {

    private final Logger log = LoggerFactory.getLogger(InterventionResource.class);

    private static final String ENTITY_NAME = "intervention";

    private final InterventionRepository interventionRepository;
    private final TrialService trialService;

    public InterventionResource(InterventionRepository interventionRepository,
                                TrialService trialService) {
        this.interventionRepository = interventionRepository;
        this.trialService = trialService;
    }

    /**
     * POST  /interventions : Create a new intervention.
     *
     * @param intervention the intervention to create
     * @return the ResponseEntity with status 201 (Created) and with body the new intervention, or with status 400 (Bad Request) if the intervention has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/interventions")
    @Timed
    public ResponseEntity<Intervention> createIntervention(@Valid @RequestBody Intervention intervention) throws URISyntaxException {
        log.debug("REST request to save Intervention : {}", intervention);
        if (intervention.getId() != null) {
            throw new BadRequestAlertException("A new intervention cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Intervention result = interventionRepository.save(intervention);
        return ResponseEntity.created(new URI("/api/interventions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /interventions : Updates an existing intervention.
     *
     * @param intervention the intervention to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated intervention,
     * or with status 400 (Bad Request) if the intervention is not valid,
     * or with status 500 (Internal Server Error) if the intervention couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/interventions")
    @Timed
    public ResponseEntity<Intervention> updateIntervention(@Valid @RequestBody Intervention intervention) throws URISyntaxException {
        log.debug("REST request to update Intervention : {}", intervention);
        if (intervention.getId() == null) {
            return createIntervention(intervention);
        }
        Intervention result = interventionRepository.save(intervention);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, intervention.getId()))
            .body(result);
    }

    /**
     * GET  /interventions : get all the interventions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of interventions in body
     */
    @GetMapping("/interventions")
    @Timed
    public List<Intervention> getAllInterventions() {
        log.debug("REST request to get all Interventions");
        return interventionRepository.findAll();
        }

    /**
     * GET  /interventions/:id : get the "id" intervention.
     *
     * @param id the id of the intervention to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the intervention, or with status 404 (Not Found)
     */
    @GetMapping("/interventions/{id}")
    @Timed
    public ResponseEntity<Intervention> getIntervention(@PathVariable String id) {
        log.debug("REST request to get Intervention : {}", id);
        Intervention intervention = interventionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(intervention));
    }

    /**
     * DELETE  /interventions/:id : delete the "id" intervention.
     *
     * @param id the id of the intervention to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/interventions/{id}")
    @Timed
    public ResponseEntity<Void> deleteIntervention(@PathVariable String id) {
        log.debug("REST request to delete Intervention : {}", id);
        interventionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * GET  /trial:id/interventions : get the "id" trial to get the interventions.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and the list of trialCentres in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/trials/{id}/interventions")
    @Timed
    public ResponseEntity<List<Intervention>> getInterventionsForTrial(@PathVariable String id, @ApiParam Pageable pageable) {
        log.debug("REST request to get Intervention for Trial : {}", id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }
        Page<Intervention> page = new PageImpl<>(new ArrayList<>(trial.getInterventions()), pageable, trial.getInterventions().size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/interventions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /trials/id/interventions : Add a new intervention for trial.
     *
     * @param intervention the Intervention to create
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trials/{id}/interventions")
    @Timed
    public ResponseEntity<Intervention> addInterventionForTrial(@PathVariable String id, @Valid @RequestBody Intervention intervention) throws URISyntaxException {
        log.debug("REST request to add intervention : {} for Trial : {}", intervention, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }
        Intervention result = interventionRepository.save(intervention);
        // now also save trial
        trial.addInterventions(result);
        trialService.save(trial);
        return ResponseEntity.created(new URI("/api/interventions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * PUT  /trials/id/interventions : Add a new intervention for trial.
     *
     * @param intervention the Intervention to create
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trials/{id}/interventions")
    @Timed
    public ResponseEntity<Intervention> updateInterventionForTrial(@PathVariable String id, @Valid @RequestBody Intervention intervention) throws URISyntaxException {
        log.debug("REST request to add intervention : {} for Trial : {}", intervention, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        Intervention result = interventionRepository.findOne(intervention.getId());
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No intervention found for trial")).build();
        }

        // save intervention and now update trial
        result = interventionRepository.save(intervention);
        // now also save trial
        trial.updateIntervention(result.getId(), result);
        trialService.save(trial);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trial.getId()))
                .body(result);
    }

    /**
     * DELETE  /trials/id/interventions : delete intervention for trial.
     *
     * @param interventionId the id of Intervention to delete
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trials/{id}/interventions")
    @Timed
    public ResponseEntity<Intervention> removeInterventionForTrial(@PathVariable String id, @PathVariable String interventionId) throws URISyntaxException {
        log.debug("REST request to add intervention : {} for Trial : {}", interventionId, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        Intervention result = interventionRepository.findOne(interventionId);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No intervention found for trial")).build();
        }

        // now also save trial
        trial.removeInterventions(result);
        trialService.save(trial);
        // remove intervention
        interventionRepository.delete(result);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trial.getId()))
                .body(result);
    }
}
