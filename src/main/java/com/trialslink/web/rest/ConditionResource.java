package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Condition;
import com.trialslink.domain.Trial;
import com.trialslink.service.ConditionService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Condition.
 */
@RestController
@RequestMapping("/api")
public class ConditionResource {

    private final Logger log = LoggerFactory.getLogger(ConditionResource.class);

    private static final String ENTITY_NAME = "condition";

    private final ConditionService conditionService;
    private final TrialService trialService;

    public ConditionResource(ConditionService conditionService, TrialService trialService) {
        this.conditionService = conditionService;
        this.trialService = trialService;
    }

    /**
     * POST  /conditions : Create a new condition.
     *
     * @param condition the condition to create
     * @return the ResponseEntity with status 201 (Created) and with body the new condition, or with status 400 (Bad Request) if the condition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/conditions")
    @Timed
    public ResponseEntity<Condition> createCondition(@Valid @RequestBody Condition condition) throws URISyntaxException {
        log.debug("REST request to save Condition : {}", condition);
        if (condition.getId() != null) {
            throw new BadRequestAlertException("A new condition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Condition result = conditionService.save(condition);
        return ResponseEntity.created(new URI("/api/conditions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * PUT  /conditions : Updates an existing condition.
     *
     * @param condition the condition to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated condition,
     * or with status 400 (Bad Request) if the condition is not valid,
     * or with status 500 (Internal Server Error) if the condition couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/conditions")
    @Timed
    public ResponseEntity<Condition> updateCondition(@Valid @RequestBody Condition condition) throws URISyntaxException {
        log.debug("REST request to update Condition : {}", condition);
        if (condition.getId() == null) {
            return createCondition(condition);
        }
        Condition result = conditionService.save(condition);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, condition.getId()))
                .body(result);
    }

    /**
     * GET  /conditions : get all the conditions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of conditions in body
     */
    @GetMapping("/conditions")
    @Timed
    public ResponseEntity<List<Condition>> getAllConditions(@ApiParam Pageable pageable) {
        log.debug("REST request to get all Conditions");
        Page<Condition> page = conditionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/conditions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /conditions/:id : get the "id" condition.
     *
     * @param id the id of the condition to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the condition, or with status 404 (Not Found)
     */
    @GetMapping("/conditions/{id}")
    @Timed
    public ResponseEntity<Condition> getCondition(@PathVariable String id) {
        log.debug("REST request to get Condition : {}", id);
        Condition condition = conditionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(condition));
    }

    /**
     * DELETE  /conditions/:id : delete the "id" condition.
     *
     * @param id the id of the condition to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/conditions/{id}")
    @Timed
    public ResponseEntity<Void> deleteCondition(@PathVariable String id) {
        log.debug("REST request to delete Condition : {}", id);
        conditionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }


    /**
     * GET  /trial:id/conditions : get the "id" trial to get the conditions.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and the list of conditions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/trials/{id}/conditions")
    @Timed
    public ResponseEntity<List<Condition>> getConditionsForTrial(@PathVariable String id, @ApiParam Pageable pageable) {
        log.debug("REST request to get Intervention for Trial : {}", id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }
        Page<Condition> page = conditionService.findAllForTrial(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/conditions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /trials/id/conditions : Add a new conditions for trial.
     *
     * @param condition the condition to create
     * @param id        the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trials/{id}/conditions")
    @Timed
    public ResponseEntity<Condition> addConditionForTrial(@PathVariable String id, @Valid @RequestBody Condition condition) throws URISyntaxException {
        log.debug("REST request to add condition : {} for Trial : {}", condition, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }
        condition.setTrialId(id);
        Condition result = conditionService.save(condition);
        // now also save trial
        trial = trial.addConditions(result);
        trialService.save(trial);
        return ResponseEntity.created(new URI("/api/conditions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * PUT  /trials/id/conditions : Update a condition for trial.
     *
     * @param condition the Condition to update
     * @param id        the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trials/{id}/conditions")
    @Timed
    public ResponseEntity<Condition> updateInterventionForTrial(@PathVariable String id, @Valid @RequestBody Condition condition) throws URISyntaxException {
        log.debug("REST request to add intervention : {} for Trial : {}", condition, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        Condition result = conditionService.findOne(condition.getId());
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No intervention found for trial")).build();
        }

        // save intervention and now update trial
        result = conditionService.save(condition);
        // now also save trial
        trial = trial.updateCondition(result.getId(), result);
        trialService.save(trial);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trial.getId()))
                .body(result);
    }

    /**
     * DELETE  /trials/id/conditions/conditionId : delete condition for trial.
     *
     * @param conditionId the Condition id to delete
     * @param id          the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trials/{id}/conditions/{conditionId}")
    @Timed
    public ResponseEntity<Void> removeConditionForTrial(@PathVariable String id, @PathVariable String conditionId) throws URISyntaxException {
        log.debug("REST request to delete condition : {} for Trial : {}", conditionId, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        Condition result = conditionService.findOne(conditionId);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No condition found for trial")).build();
        }

        conditionService.delete(conditionId);
        // now also save trial
        trial = trial.removeConditions(result);
        trial = trialService.save(trial);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * DELETE  /trials/id/conditions : delete all conditions for trial.
     *
     * @param id          the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trials/{id}/conditions")
    @Timed
    public ResponseEntity<Void> removeAllConditionsForTrial(@PathVariable String id) throws URISyntaxException {
        log.debug("REST request to delete all conditions for Trial : {}", id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        conditionService.deleteAllForTrial(id);
        // now also save trial
        trial.setConditions(new HashSet<>());
        trialService.save(trial);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/conditions?query=:query : search for the conditions corresponding
     * to the query.
     *
     * @param query    the query of the condition search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/conditions")
    @Timed
    public ResponseEntity<List<Condition>> searchConditions(@RequestParam String query, @ApiParam Pageable pageable)
            throws URISyntaxException {
        log.debug("REST request to search for a page of Condition for query {}", query);
        Page<Condition> page = conditionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/conditions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
