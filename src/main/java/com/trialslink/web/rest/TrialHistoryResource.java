package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.TrialHistory;
import com.trialslink.service.TrialHistoryService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TrialHistory.
 */
@RestController
@RequestMapping("/api")
public class TrialHistoryResource {

    private final Logger log = LoggerFactory.getLogger(TrialHistoryResource.class);

    private static final String ENTITY_NAME = "trialHistory";

    private final TrialHistoryService trialHistoryService;

    public TrialHistoryResource(TrialHistoryService trialHistoryService) {
        this.trialHistoryService = trialHistoryService;
    }

    /**
     * GET  /trial-histories/:id : get the "id" trialHistory.
     *
     * @param id the id of the trialHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trialHistory, or with status 404 (Not Found)
     */
    @GetMapping("/trial-histories/{id}")
    @Timed
    public ResponseEntity<TrialHistory> getTrialHistory(@PathVariable String id) {
        log.debug("REST request to get TrialHistory : {}", id);
        TrialHistory trialHistory = trialHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trialHistory));
    }

    /**
     * POST  /trial-histories : Create a new trialHistory.
     *
     * @param trialHistory the trialHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trialHistory, or with status 400 (Bad Request) if the trialHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PostMapping("/trial-histories")
    @Timed
    public ResponseEntity<TrialHistory> createTrialHistory(@RequestBody TrialHistory trialHistory) throws URISyntaxException {
        log.debug("REST request to save TrialHistory : {}", trialHistory);
        if (trialHistory.getId() != null) {
            throw new BadRequestAlertException("A new trialHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TrialHistory result = trialHistoryService.save(trialHistory);
        return ResponseEntity.created(new URI("/api/trial-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }*/

    /**
     * PUT  /trial-histories : Updates an existing trialHistory.
     *
     * @param trialHistory the trialHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trialHistory,
     * or with status 400 (Bad Request) if the trialHistory is not valid,
     * or with status 500 (Internal Server Error) if the trialHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PutMapping("/trial-histories")
    @Timed
    public ResponseEntity<TrialHistory> updateTrialHistory(@RequestBody TrialHistory trialHistory) throws URISyntaxException {
        log.debug("REST request to update TrialHistory : {}", trialHistory);
        if (trialHistory.getId() == null) {
            return createTrialHistory(trialHistory);
        }
        TrialHistory result = trialHistoryService.save(trialHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trialHistory.getId().toString()))
            .body(result);
    }*/

    /**
     * GET  /trial-histories : get all the trialHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of trialHistories in body
     */
    /*@GetMapping("/trial-histories")
    @Timed
    public ResponseEntity<List<TrialHistory>> getAllTrialHistories(Pageable pageable) {
        log.debug("REST request to get a page of TrialHistories");
        Page<TrialHistory> page = trialHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trial-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }*/

    /**
     * DELETE  /trial-histories/:id : delete the "id" trialHistory.
     *
     * @param id the id of the trialHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    /*@DeleteMapping("/trial-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteTrialHistory(@PathVariable String id) {
        log.debug("REST request to delete TrialHistory : {}", id);
        trialHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }*/
}
