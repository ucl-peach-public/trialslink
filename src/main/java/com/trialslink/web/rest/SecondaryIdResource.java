package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.SecondaryId;

import com.trialslink.repository.SecondaryIdRepository;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SecondaryId.
 */
@RestController
@RequestMapping("/api")
public class SecondaryIdResource {

    private final Logger log = LoggerFactory.getLogger(SecondaryIdResource.class);

    private static final String ENTITY_NAME = "secondaryId";

    private final SecondaryIdRepository secondaryIdRepository;

    public SecondaryIdResource(SecondaryIdRepository secondaryIdRepository) {
        this.secondaryIdRepository = secondaryIdRepository;
    }

    /**
     * POST  /secondary-ids : Create a new secondaryId.
     *
     * @param secondaryId the secondaryId to create
     * @return the ResponseEntity with status 201 (Created) and with body the new secondaryId, or with status 400 (Bad Request) if the secondaryId has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/secondary-ids")
    @Timed
    public ResponseEntity<SecondaryId> createSecondaryId(@Valid @RequestBody SecondaryId secondaryId) throws URISyntaxException {
        log.debug("REST request to save SecondaryId : {}", secondaryId);
        if (secondaryId.getId() != null) {
            throw new BadRequestAlertException("A new secondaryId cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SecondaryId result = secondaryIdRepository.save(secondaryId);
        return ResponseEntity.created(new URI("/api/secondary-ids/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /secondary-ids : Updates an existing secondaryId.
     *
     * @param secondaryId the secondaryId to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated secondaryId,
     * or with status 400 (Bad Request) if the secondaryId is not valid,
     * or with status 500 (Internal Server Error) if the secondaryId couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/secondary-ids")
    @Timed
    public ResponseEntity<SecondaryId> updateSecondaryId(@Valid @RequestBody SecondaryId secondaryId) throws URISyntaxException {
        log.debug("REST request to update SecondaryId : {}", secondaryId);
        if (secondaryId.getId() == null) {
            return createSecondaryId(secondaryId);
        }
        SecondaryId result = secondaryIdRepository.save(secondaryId);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, secondaryId.getId().toString()))
            .body(result);
    }

    /**
     * GET  /secondary-ids : get all the secondaryIds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of secondaryIds in body
     */
    @GetMapping("/secondary-ids")
    @Timed
    public List<SecondaryId> getAllSecondaryIds() {
        log.debug("REST request to get all SecondaryIds");
        return secondaryIdRepository.findAll();
        }

    /**
     * GET  /secondary-ids/:id : get the "id" secondaryId.
     *
     * @param id the id of the secondaryId to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the secondaryId, or with status 404 (Not Found)
     */
    @GetMapping("/secondary-ids/{id}")
    @Timed
    public ResponseEntity<SecondaryId> getSecondaryId(@PathVariable String id) {
        log.debug("REST request to get SecondaryId : {}", id);
        SecondaryId secondaryId = secondaryIdRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(secondaryId));
    }

    /**
     * DELETE  /secondary-ids/:id : delete the "id" secondaryId.
     *
     * @param id the id of the secondaryId to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/secondary-ids/{id}")
    @Timed
    public ResponseEntity<Void> deleteSecondaryId(@PathVariable String id) {
        log.debug("REST request to delete SecondaryId : {}", id);
        secondaryIdRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
