package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.TrialImport;
import com.trialslink.security.SecurityUtils;
import com.trialslink.service.TrialImportService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TrialImport.
 */
@RestController
@RequestMapping("/api")
public class TrialImportResource {

    private final Logger log = LoggerFactory.getLogger(TrialImportResource.class);

    private static final String ENTITY_NAME = "trialImport";

    private final TrialImportService trialImportService;


    public TrialImportResource(TrialImportService trialImportService) {
        this.trialImportService = trialImportService;
    }

    /**
     * POST  /trial-imports : Create a new trialImport.
     *
     * @param trialImport the trialImport to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trialImport, or with status 400 (Bad Request) if the trialImport has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trial-imports")
    @Timed
    public ResponseEntity<TrialImport> createTrialImport(@Valid @RequestBody TrialImport trialImport) throws URISyntaxException, ParseException {
        log.debug("REST request to save TrialImport : {}", trialImport);

        if (trialImport.getId() != null) {
            throw new BadRequestAlertException("A new trialImport cannot already have an ID", ENTITY_NAME, "idexists");
        }

        //save the trialImport to database and send to message queue
        TrialImport result = trialImportService.save(trialImport);

        return ResponseEntity.created(new URI("/api/trial-imports/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /trial-imports : Updates an existing trialImport.
     *
     * @param trialImport the trialImport to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trialImport,
     * or with status 400 (Bad Request) if the trialImport is not valid,
     * or with status 500 (Internal Server Error) if the trialImport couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PutMapping("/trial-imports")
    @Timed
    public ResponseEntity<TrialImport> updateTrialImport(@Valid @RequestBody TrialImport trialImport) throws URISyntaxException, ParseException {
        // log.debug("REST request to update TrialImport : {}", trialImport);
        // if (trialImport.getId() == null) {
        //     return createTrialImport(trialImport);
        // }
        // TrialImport result = trialImportService.save(trialImport);
        // return ResponseEntity.ok()
        //     .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trialImport.getId().toString()))
        //     .body(result);
    }*/

    /**
     * GET  /trial-imports : get all the trialImports belong to current user.
     *
     * @param pageable the pagination information
     * @requestHeader header of the request
     * @return the ResponseEntity with status 200 (OK) and the list of trialImports in body
     */
    @GetMapping("/trial-imports")
    @Timed
    public ResponseEntity<List<TrialImport>> getAllTrialImports(Pageable pageable) {
        log.debug("REST request to get a page of TrialImports");

        Page<TrialImport> page = trialImportService.findAllByUserId(SecurityUtils.getCurrentUserLogin().get(), pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trial-imports");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /trial-imports/:id : get the "id" trialImport.
     *
     * @param id the id of the trialImport to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trialImport, or with status 404 (Not Found)
     */
    @GetMapping("/trial-imports/{id}")
    @Timed
    public ResponseEntity<TrialImport> getTrialImport(@PathVariable String id) {
        log.debug("REST request to get TrialImport : {}", id);
        TrialImport trialImport = trialImportService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trialImport));
    }

    /**
     * DELETE  /trial-imports/:id : delete the "id" trialImport.
     *
     * @param id the id of the trialImport to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    /*@DeleteMapping("/trial-imports/{id}")
    @Timed
    public ResponseEntity<Void> deleteTrialImport(@PathVariable String id) {
        // log.debug("REST request to delete TrialImport : {}", id);
        // trialImportService.delete(id);
        // return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }*/

    /*@DeleteMapping("/trial-imports/all")
    @Timed
    public ResponseEntity<Void> deleteAllTrialImport() {
        // log.debug("REST request to delete all TrialImport");
        // trialImportService.deleteAllByIdNotNull();
        // return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, "OK")).build();
    }*/
}
