package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.OutcomeMeasure;
import com.trialslink.service.OutcomeMeasureService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OutcomeMeasure.
 */
@RestController
@RequestMapping("/api")
public class OutcomeMeasureResource {

    private final Logger log = LoggerFactory.getLogger(OutcomeMeasureResource.class);

    private static final String ENTITY_NAME = "outcomeMeasure";

    private final OutcomeMeasureService outcomeMeasureService;

    public OutcomeMeasureResource(OutcomeMeasureService outcomeMeasureService) {
        this.outcomeMeasureService = outcomeMeasureService;
    }

    /**
     * POST  /outcome-measures : Create a new outcomeMeasure.
     *
     * @param outcomeMeasure the outcomeMeasure to create
     * @return the ResponseEntity with status 201 (Created) and with body the new outcomeMeasure, or with status 400 (Bad Request) if the outcomeMeasure has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/outcome-measures")
    @Timed
    public ResponseEntity<OutcomeMeasure> createOutcomeMeasure(@Valid @RequestBody OutcomeMeasure outcomeMeasure) throws URISyntaxException {
        log.debug("REST request to save OutcomeMeasure : {}", outcomeMeasure);
        if (outcomeMeasure.getId() != null) {
            throw new BadRequestAlertException("A new outcomeMeasure cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OutcomeMeasure result = outcomeMeasureService.save(outcomeMeasure);
        return ResponseEntity.created(new URI("/api/outcome-measures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /outcome-measures : Updates an existing outcomeMeasure.
     *
     * @param outcomeMeasure the outcomeMeasure to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated outcomeMeasure,
     * or with status 400 (Bad Request) if the outcomeMeasure is not valid,
     * or with status 500 (Internal Server Error) if the outcomeMeasure couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/outcome-measures")
    @Timed
    public ResponseEntity<OutcomeMeasure> updateOutcomeMeasure(@Valid @RequestBody OutcomeMeasure outcomeMeasure) throws URISyntaxException {
        log.debug("REST request to update OutcomeMeasure : {}", outcomeMeasure);
        if (outcomeMeasure.getId() == null) {
            return createOutcomeMeasure(outcomeMeasure);
        }
        OutcomeMeasure result = outcomeMeasureService.save(outcomeMeasure);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, outcomeMeasure.getId().toString()))
            .body(result);
    }

    /**
     * GET  /outcome-measures : get all the outcomeMeasures.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of outcomeMeasures in body
     */
    @GetMapping("/outcome-measures")
    @Timed
    public ResponseEntity<List<OutcomeMeasure>> getAllOutcomeMeasures(Pageable pageable) {
        log.debug("REST request to get a page of OutcomeMeasures");
        Page<OutcomeMeasure> page = outcomeMeasureService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/outcome-measures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /outcome-measures/:id : get the "id" outcomeMeasure.
     *
     * @param id the id of the outcomeMeasure to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the outcomeMeasure, or with status 404 (Not Found)
     */
    @GetMapping("/outcome-measures/{id}")
    @Timed
    public ResponseEntity<OutcomeMeasure> getOutcomeMeasure(@PathVariable String id) {
        log.debug("REST request to get OutcomeMeasure : {}", id);
        OutcomeMeasure outcomeMeasure = outcomeMeasureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(outcomeMeasure));
    }

    /**
     * DELETE  /outcome-measures/:id : delete the "id" outcomeMeasure.
     *
     * @param id the id of the outcomeMeasure to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/outcome-measures/{id}")
    @Timed
    public ResponseEntity<Void> deleteOutcomeMeasure(@PathVariable String id) {
        log.debug("REST request to delete OutcomeMeasure : {}", id);
        outcomeMeasureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
