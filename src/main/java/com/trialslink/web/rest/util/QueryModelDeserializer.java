package com.trialslink.web.rest.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A custom Jackson deserializer to handle Infobutton query params
 * Created by Jay Kola on 09/03/19.
 */
public class QueryModelDeserializer extends StdDeserializer<QueryModel>{

    protected QueryModelDeserializer(Class<?> vc) {
        super(vc);
    }

    public QueryModelDeserializer() {
        this(null);
    }

    @Override
    public QueryModel deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);

        // create placeholders
        QueryModel queryModel = new QueryModel();

        List<String> conditions = new ArrayList<>();
        List<String> locations = new ArrayList<>();
        List<String> phases = new ArrayList<>();
        List<String> statuses = new ArrayList<>();
        List<String> siteStatuses = new ArrayList<>();
        List<String> conditionStatuses = new ArrayList<>();
        List<String> types = new ArrayList<>();
        List<String> genders = new ArrayList<>();
        List<String> areas = new ArrayList<>();
        List<String> sites = new ArrayList<>();

        // process fields
        node.fieldNames().forEachRemaining(field -> {
            if("mainSearchCriteria.v.c".equalsIgnoreCase(field)) {
                node.findValues("mainSearchCriteria.v.c").forEach(code -> {
                    code.forEach(item -> {
                        conditions.add(item.asText());
                    });
                });
            } else if("conditions".equalsIgnoreCase(field)) {
                node.findValues("conditions").forEach(code -> {
                    code.forEach(item -> {
                        conditions.add(item.asText());
                    });
                });
            } else if("locations".equalsIgnoreCase(field)) {
                node.findValues("locations").forEach(code -> {
                    code.forEach(item -> {
                        locations.add(item.asText());
                    });
                });
            } else if("phases".equalsIgnoreCase(field)) {
                node.findValues("phases").forEach(code -> {
                    code.forEach(item -> {
                        phases.add(item.asText());
                    });
                });
            } else if("genders".equalsIgnoreCase(field)) {
                node.findValues("genders").forEach(code -> {
                    code.forEach(item -> {
                        genders.add(item.asText());
                    });
                });
            } else if("types".equalsIgnoreCase(field)) {
                node.findValues("types").forEach(code -> {
                    code.forEach(item -> {
                        types.add(item.asText());
                    });
                });
            } else if("statuses".equalsIgnoreCase(field)) {
                node.findValues("statuses").forEach(code -> {
                    code.forEach(item -> {
                        statuses.add(item.asText());
                    });
                });
            } else if("siteStatuses".equalsIgnoreCase(field)) {
                node.findValues("siteStatuses").forEach(code -> {
                    code.forEach(item -> {
                        siteStatuses.add(item.asText());
                    });
                });
            } else if("areas".equalsIgnoreCase(field)) {
                node.findValues("areas").forEach(code -> {
                    code.forEach(item -> {
                        areas.add(item.asText());
                    });
                });
            } else if("sites".equalsIgnoreCase(field)) {
                node.findValues("sites").forEach(code -> {
                    code.forEach(item -> {
                        sites.add(item.asText());
                    });
                });
            } else if("conditionStatuses".equalsIgnoreCase(field)) {
                node.findValues("conditionStatuses").forEach(code -> {
                    code.forEach(item -> {
                        conditionStatuses.add(item.asText());
                    });
                });
            } else if("age".equalsIgnoreCase(field)) {

                if(! node.get("age").isNull()) {
                    int age = node.get("age").asInt();
                    queryModel.setAge(age);
                }
            } else if("token".equalsIgnoreCase(field)) {

                String token = node.get("token").asText();
                if(token != null) {
                    queryModel.setToken((token));
                }
            }
            else if("includeAncestors".equalsIgnoreCase(field)) {

                boolean includeAncestors = node.get("includeAncestors").asBoolean();
                queryModel.setIncludeAncestors((includeAncestors));
            }
        });


        // now assign attributes to model
        queryModel.setConditions(conditions);
        queryModel.setLocations(locations);
        queryModel.setPhases(phases);
        queryModel.setConditionStatuses(conditionStatuses);
        queryModel.setGenders(genders);
        queryModel.setTypes(types);
        queryModel.setStatuses(statuses);
        queryModel.setSiteStatuses(siteStatuses);
        queryModel.setAreas(areas);
        queryModel.setSites(sites);

        return queryModel;
    }
}
