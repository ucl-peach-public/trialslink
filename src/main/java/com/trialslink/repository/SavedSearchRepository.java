package com.trialslink.repository;

import com.trialslink.domain.SavedSearch;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the SavedSearch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SavedSearchRepository extends MongoRepository<SavedSearch,String> {
    SavedSearch findOneByHash(String hash);
    Page<SavedSearch> findAllByCreatedBy(String userID, Pageable pageable);

    @Override
    List<SavedSearch> findAll();
}
