package com.trialslink.repository;

import com.trialslink.domain.Trial;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Spring Data MongoDB repository for the Trial entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrialRepository extends MongoRepository<Trial,String> {
    Trial findOneByShortName(String shortName);

    Trial findOneByIdInfoNctId(String nctId);

    Trial findOneByCreatedByAndId(String username, String id);

    Set<Trial> findAllByCategory(String category);

    Trial findOneByIdInfoId(String edgeId);
}
