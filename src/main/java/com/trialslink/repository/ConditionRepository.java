package com.trialslink.repository;

import com.trialslink.domain.Condition;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Condition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConditionRepository extends MongoRepository<Condition, String> {

    Condition findOneByLabel(String label);

    Page<Condition> findByTrialId(String trialId, Pageable pageable);

    Long deleteByTrialId(String trialId);

    Condition findOneByTrialIdAndCodeAndStatusCode(String trialId, String code, String statusCode);
}
