package com.trialslink.repository;

import com.trialslink.domain.TrialImport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;


/*
 * Spring Data MongoDB repository for the TrialImport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrialImportRepository extends MongoRepository<TrialImport, String> {

    // exclude trials and failed trials when fetching a list of trialImport objects from the database
    @Query(value = "{}", fields = "{ 'trials' : 0, 'failed_trials': 0 }")
    Page<TrialImport> findAllByCreatedBy(String userId, Pageable pageable);

    void deleteAllByIdNotNull();
}
