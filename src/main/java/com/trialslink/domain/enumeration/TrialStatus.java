package com.trialslink.domain.enumeration;

/**
 * The TrialStatus enumeration.
 */
public enum TrialStatus {
    RECRUITING,  SUSPENDED,  STOPPED,  COMPLETED,  DRAFT,  NOT_YET_RECRUITING,  ENROLLING_BY_INVITATION,
    ACTIVE_NOT_RECRUITING,  TERMINATED,  WITHDRAWN, FOLLOWUP, FEASIBILITY,  UNKNOWN
}
