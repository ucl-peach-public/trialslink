package com.trialslink.domain;

import com.trialslink.domain.enumeration.PrivilegeRequestStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PrivilegeRequest.
 */
@Document(collection = "privilege_request")
public class PrivilegeRequest extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("status")
    private PrivilegeRequestStatus status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PrivilegeRequestStatus getStatus() {
        return status;
    }

    public PrivilegeRequest status(PrivilegeRequestStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(PrivilegeRequestStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PrivilegeRequest privilegeRequest = (PrivilegeRequest) o;
        if (privilegeRequest.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), privilegeRequest.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PrivilegeRequest{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
                ", created_by='" + getCreatedBy() + "'" +
                ", updated_by='" + getLastModifiedBy() + "'" +
                ", created_at='" + getCreatedDate() + "'" +
                ", updated_at='" + getLastModifiedDate() + "'" +
            "}";
    }
}
