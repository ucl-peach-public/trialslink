package com.trialslink.domain;

import com.trialslink.domain.enumeration.Sex;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Characteristic.
 */
@Document(collection = "characteristic")
public class Characteristic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("criteria")
    private String criteria;

    @Field("code")
    private String code;

    @NotNull
    @Field("sex")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private Sex sex;

    @Field("based_on_gender")
    private Boolean basedOnGender;

    @Field("accept_healthy_volunteers")
    private Boolean acceptHealthyVolunteers;

    @Field("min_age")
    private Age minAge;

    @Field("max_age")
    private Age maxAge;

    @Field("trial_id")
    private String trialId;

    @Field
    private Boolean exclusive;


    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    public Boolean isExclusive() { return exclusive; }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCriteria() {
        return criteria;
    }

    public Characteristic criteria(String criteria) {
        this.criteria = criteria;
        return this;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getCode() {
        return code;
    }

    public Characteristic code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Sex getSex() {
        return sex;
    }

    public Characteristic sex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Boolean isBasedOnGender() {
        return basedOnGender;
    }

    public Characteristic basedOnGender(Boolean basedOnGender) {
        this.basedOnGender = basedOnGender;
        return this;
    }

    public void setBasedOnGender(Boolean basedOnGender) {
        this.basedOnGender = basedOnGender;
    }

    public Boolean isAcceptHealthyVolunteers() {
        return acceptHealthyVolunteers;
    }

    public Characteristic acceptHealthyVolunteers(Boolean acceptHealthyVolunteers) {
        this.acceptHealthyVolunteers = acceptHealthyVolunteers;
        return this;
    }

    public void setAcceptHealthyVolunteers(Boolean acceptHealthyVolunteers) {
        this.acceptHealthyVolunteers = acceptHealthyVolunteers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Age getMinAge() {
        return minAge;
    }

    public void setMinAge(Age minAge) {
        this.minAge = minAge;
    }

    public Age getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Age maxAge) {
        this.maxAge = maxAge;
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Characteristic characteristic = (Characteristic) o;
        if (characteristic.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), characteristic.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Characteristic{" +
            "id=" + getId() +
            ", criteria='" + getCriteria() + "'" +
            ", code='" + getCode() + "'" +
            ", sex='" + getSex() + "'" +
            ", basedOnGender='" + isBasedOnGender() + "'" +
            ", acceptHealthyVolunteers='" + isAcceptHealthyVolunteers() + "'" +
            ", isExclusive='" + isExclusive() + "'" +
            "}";
    }
}
