package com.trialslink.domain;

/**
 * A Visitor object for  {@link Trial}s.
 */
public interface Visitor {

    Trial visit(Trial trial);
}
