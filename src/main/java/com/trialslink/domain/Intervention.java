package com.trialslink.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Intervention.
 */
@Document(collection = "intervention")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "intervention", type = "intervention")
@Setting(settingPath = "/config/elasticsearch/settings/index-settings.json")
//@Mapping(mappingPath = "/config/elasticsearch/mappings/intervention-mappings.json")
@XStreamAlias(value = "intervention")
public class Intervention implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("label")
    @XStreamAlias(value = "intervention_name")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String, analyzer = "nameAnalyzer")
    private String label;

    @Field("system")
    private String system;

    @Field("code")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String code;

    @Field("type")
    @XStreamAlias(value = "intervention_type")
    private String type;

    @Field("description")
    @XStreamAlias(value = "description")
    private String description;

    @Field("armLabelGroup")
    @XStreamAlias(value = "arm_group_label")
    private String armLabelGroup;

    @Field("otherName")
    @XStreamAlias(value = "other_name")
    private String otherName;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Intervention label(String label) {
        this.label = label;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public Intervention system(String system) {
        this.system = system;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Intervention code(String code) {
        this.code = code;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Intervention type(String type) {
        this.type = type;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArmLabelGroup() {
        return armLabelGroup;
    }

    public void setArmLabelGroup(String armLabelGroup) {
        this.armLabelGroup = armLabelGroup;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Intervention intervention = (Intervention) o;
        if (intervention.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), intervention.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Intervention{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", system='" + getSystem() + "'" +
            ", code='" + getCode() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
