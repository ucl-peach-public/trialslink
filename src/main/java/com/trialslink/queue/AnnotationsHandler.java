package com.trialslink.queue;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Trial;
import com.trialslink.service.AnnotationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * A handler for annotations queue
 * Created by jaykola on 07/06/2019.
 */
@Component
@RabbitListener(bindings = @QueueBinding(value = @org.springframework.amqp.rabbit.annotation.Queue(value = TrialslinkApp.annotationsQueue, durable = "true"), exchange = @Exchange(value = TrialslinkApp.exchange, autoDelete = "true"), key = "annotation"))
public class AnnotationsHandler {

    private final Logger log = LoggerFactory.getLogger(AnnotationsHandler.class);
    private final AnnotationService annotationService;

    public AnnotationsHandler(AnnotationService annotationService) {
        this.annotationService = annotationService;
    }

    @RabbitHandler
    public void receiveMessage(@Payload Trial trial){
        log.debug("Received trial for annotation", trial);
        //send the trial for annotation
        annotationService.annotateTrial(trial);
    }
}
