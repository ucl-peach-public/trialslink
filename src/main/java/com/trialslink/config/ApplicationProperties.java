package com.trialslink.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Properties specific to Trialslink.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Validated
public class ApplicationProperties {

    private boolean importTrials;
    private boolean importCodes;
    private String importFilePath;
    private String branding;
    private String dateFormat;
    private boolean preferOmnisearch;
    private boolean allowRegistrations;
    private boolean showLanguageSwitcher;
    private Ribbon ribbon  = new Ribbon();
    private boolean useLocalStorage;
    private ContactDetails contactDetails = new ContactDetails();
    private boolean singleSiteInstance;
    @NotNull @Valid
    private List<String> allowedFilters = new ArrayList<>();

    public boolean isImportTrials() {
        return importTrials;
    }

    public void setImportTrials(boolean importTrials) {
        this.importTrials = importTrials;
    }

    public boolean isImportCodes() {
        return importCodes;
    }

    public void setImportCodes(boolean importCodes) {
        this.importCodes = importCodes;
    }

    public String getImportFilePath() {
        return importFilePath;
    }

    public void setImportFilePath(String importFilePath) {
        this.importFilePath = importFilePath;
    }

    public String getBranding() {
        return branding;
    }

    public void setBranding(String branding) {
        this.branding = branding;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isPreferOmnisearch() {
        return preferOmnisearch;
    }

    public void setPreferOmnisearch(boolean preferOmnisearch) {
        this.preferOmnisearch = preferOmnisearch;
    }

    public boolean isAllowRegistrations() {
        return allowRegistrations;
    }

    public void setAllowRegistrations(boolean allowRegistrations) {
        this.allowRegistrations = allowRegistrations;
    }

    public boolean isShowLanguageSwitcher() {
        return showLanguageSwitcher;
    }

    public void setShowLanguageSwitcher(boolean showLanguageSwitcher) {
        this.showLanguageSwitcher = showLanguageSwitcher;
    }

    public Ribbon getRibbon() {
        return ribbon;
    }

    public void setRibbon(Ribbon ribbon) {
        this.ribbon = ribbon;
    }

    public class Ribbon{
        private boolean display;
        private String text;

        public boolean isDisplay() {
            return display;
        }

        public void setDisplay(boolean display) {
            this.display = display;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }

    public boolean isUseLocalStorage() {
        return useLocalStorage;
    }

    public void setUseLocalStorage(boolean useLocalStorage) {
        this.useLocalStorage = useLocalStorage;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public boolean isSingleSiteInstance() {
        return singleSiteInstance;
    }

    public void setSingleSiteInstance(boolean singleSiteInstance) {
        this.singleSiteInstance = singleSiteInstance;
    }

    public List<String> getAllowedFilters() {
        return allowedFilters;
    }

    public void setAllowedFilters(List<String> allowedFilters) {
        this.allowedFilters = allowedFilters;
    }


    public class ContactDetails {

        boolean mask;
        String defaultMask;

        public boolean isMask() {
            return mask;
        }

        public void setMask(boolean mask) {
            this.mask = mask;
        }

        public String getDefaultMask() {
            return defaultMask;
        }

        public void setDefaultMask(String defaultMask) {
            this.defaultMask = defaultMask;
        }
    }
}
