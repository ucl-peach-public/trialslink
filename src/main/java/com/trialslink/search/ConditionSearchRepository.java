package com.trialslink.search;

import com.trialslink.domain.Condition;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Condition} entity.
 */
public interface ConditionSearchRepository extends ElasticsearchRepository<Condition, String> {

    Long deleteByTrialId(String trialId);
}
