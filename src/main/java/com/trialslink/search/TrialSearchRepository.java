package com.trialslink.search;

import com.trialslink.domain.Trial;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * Spring Data Elasticsearch repository for the Trial entity.
 */
public interface TrialSearchRepository extends ElasticsearchRepository<Trial, String> {
    List<Trial> findByShortNameLike(String shortName);

    List<Trial> findByShortName(String shortName);
}
