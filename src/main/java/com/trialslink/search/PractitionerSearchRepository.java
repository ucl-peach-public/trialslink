package com.trialslink.search;

import com.trialslink.domain.Practitioner;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Practitioner entity.
 */
public interface PractitionerSearchRepository extends ElasticsearchRepository<Practitioner, String> {
}
