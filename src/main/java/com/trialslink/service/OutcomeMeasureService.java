package com.trialslink.service;

import com.trialslink.domain.OutcomeMeasure;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing OutcomeMeasure.
 */
public interface OutcomeMeasureService {

    /**
     * Save a outcomeMeasure.
     *
     * @param outcomeMeasure the entity to save
     * @return the persisted entity
     */
    OutcomeMeasure save(OutcomeMeasure outcomeMeasure);

    /**
     * Get all the outcomeMeasures.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OutcomeMeasure> findAll(Pageable pageable);

    /**
     * Get the "id" outcomeMeasure.
     *
     * @param id the id of the entity
     * @return the entity
     */
    OutcomeMeasure findOne(String id);

    /**
     * Delete the "id" outcomeMeasure.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
