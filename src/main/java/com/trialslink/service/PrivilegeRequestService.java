package com.trialslink.service;

import com.trialslink.domain.PrivilegeRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PrivilegeRequest.
 */
public interface PrivilegeRequestService {

    /**
     * Save a privilegeRequest.
     *
     * @param privilegeRequest the entity to save
     * @return the persisted entity
     */
    PrivilegeRequest save(PrivilegeRequest privilegeRequest);

    /**
     *  Get all the privilegeRequests.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PrivilegeRequest> findAll(Pageable pageable);

    /**
     * Get the "id" privilegeRequest.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PrivilegeRequest findOne(String id);

    /**
     * Delete the "id" privilegeRequest.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     *  Update the privilegeRequest.
     *
     *  @param privilegeRequest the entity
     */
    PrivilegeRequest handleRequest(PrivilegeRequest privilegeRequest);

}
