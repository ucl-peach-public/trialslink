package com.trialslink.service;

import com.trialslink.domain.SavedSearch;
import com.trialslink.web.rest.util.QueryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing SavedSearch.
 */
public interface SavedSearchService {

    /**
     * Save a savedSearch.
     *
     * @param savedSearch the entity to save
     * @return the persisted entity
     */
    SavedSearch save(SavedSearch savedSearch);

//    /**
//     * Update a savedSearch.
//     *
//     * @param savedSearch the entity to save
//     * @return the persisted entity
//     */
//    SavedSearch update(SavedSearch savedSearch);

    /**
     *  Get all the savedSearches.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SavedSearch> findAll(Pageable pageable);

    /**
     * Get the "id" savedSearch.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SavedSearch findOne(String id);

    /**
     * Delete the "id" savedSearch.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
