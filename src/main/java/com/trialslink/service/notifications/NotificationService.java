package com.trialslink.service.notifications;

import com.trialslink.domain.SavedSearch;
import com.trialslink.domain.Trial;
import com.trialslink.domain.User;
import com.trialslink.repository.UserRepository;
import com.trialslink.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class NotificationService {

    private final Logger log = LoggerFactory.getLogger(NotificationService.class);
    private final UserRepository userRepo;
    private final MailService mailService;

    public NotificationService(UserRepository userRepo, MailService mailService) {
        this.userRepo = userRepo;
        this.mailService = mailService;
    }

    public void processNotification(Notifications notification) {
        log.debug("Received notification from handler");

        SavedSearch savedSearch = notification.getSavedSearch();
        ArrayList<Trial> trials = notification.getTrials();

        String userID = notification.getSavedSearch().getCreatedBy();
        Optional<User> user = userRepo.findOneById(userID);

        mailService.sendNotificationEmail(user, savedSearch, trials);
    }
}
