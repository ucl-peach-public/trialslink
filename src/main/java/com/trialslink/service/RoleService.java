package com.trialslink.service;

import com.trialslink.domain.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Interface for managing Role.
 */
public interface RoleService {

    /**
     * Save a role.
     *
     * @param role the entity to save
     * @return the persisted entity
     */
    Role save(Role role);

    /**
     * Get all the roles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Role> findAll(Pageable pageable);

    @Transactional(readOnly = true)
    List<Role> findAllAsList();

    /**
     * Get the "id" role.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Role findOne(String id);

    /**
     * Delete the "id" role.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
