package com.trialslink.service;

import com.trialslink.domain.Characteristic;
import com.trialslink.domain.Trial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Characteristic.
 */
public interface CharacteristicService {

    /**
     * Save a characteristic.
     *
     * @param characteristic the entity to save
     * @return the persisted entity
     */
    Characteristic save(Characteristic characteristic);

    /**
     * Get all the characteristics.
     *
     * @return the list of entities
     */
    List<Characteristic> findAll();

    /**
<<<<<<< HEAD
     *  Get all the characteristics.
     *
     *  @return the list of entities
     */

    Page<Characteristic> findAllForTrial(Trial trial, Pageable pageable);

    /**
     *  Get the "id" characteristic.
=======
     * Get the "id" characteristic.
>>>>>>> jhipster_upgrade
     *
     * @param id the id of the entity
     * @return the entity
     */
    Characteristic findOne(String id);

    /**
     * Delete the "id" characteristic.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    void deleteAllForTrial(String trialId);
}
