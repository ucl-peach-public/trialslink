package com.trialslink.service.impl;

import com.trialslink.service.TrialHistoryService;
import com.trialslink.domain.TrialHistory;
import com.trialslink.repository.TrialHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing TrialHistory.
 */
@Service
public class TrialHistoryServiceImpl implements TrialHistoryService{

    private final Logger log = LoggerFactory.getLogger(TrialHistoryServiceImpl.class);

    private final TrialHistoryRepository trialHistoryRepository;

    public TrialHistoryServiceImpl(TrialHistoryRepository trialHistoryRepository) {
        this.trialHistoryRepository = trialHistoryRepository;
    }

    /**
     * Save a trialHistory.
     *
     * @param trialHistory the entity to save
     * @return the persisted entity
     */
    @Override
    public TrialHistory save(TrialHistory trialHistory) {
        log.debug("Request to save TrialHistory : {}", trialHistory);
        return trialHistoryRepository.save(trialHistory);
    }

    /**
     *  Get all the trialHistories.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     * Get all the trialHistories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<TrialHistory> findAll(Pageable pageable) {
        log.debug("Request to get all TrialHistories");
        return trialHistoryRepository.findAll(pageable);
    }

    /**
     * Get one trialHistory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public TrialHistory findOne(String id) {
        log.debug("Request to get TrialHistory : {}", id);
        return trialHistoryRepository.findOne(id);
    }

    /**
     * Delete the trialHistory by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete TrialHistory : {}", id);
        trialHistoryRepository.delete(id);
    }
}
