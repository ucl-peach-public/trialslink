package com.trialslink.service;

import com.trialslink.domain.*;
import com.trialslink.repository.ConceptRepository;
import com.trialslink.repository.TrialCentreRepository;
import com.trialslink.repository.TrialRepository;
import com.trialslink.service.util.TermlexRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A concrete implementation of {@link com.trialslink.domain.Visitor} that visits {@link com.trialslink.domain.Trial}s.
 */
@Service
public class TrialVisitorService {

    private final ConceptRepository conceptRepository;
    private final ConditionService conditionService;
    private final CharacteristicService characteristicService;
    private final TrialService trialService;
    private final TrialCentreService trialCentreService;
    private final Logger log = LoggerFactory.getLogger(TrialVisitorService.class);
    private final OrganisationService organisationService;
    private final TrialCentreRepository trialCentreRepository;
    private final TrialRepository trialRepository;
    private final ContactService contactService;
    private final TermlexRestClient gateRestClient;


    public TrialVisitorService(ConceptRepository conceptRepository, ConditionService conditionService,
                               TrialService trialService, TrialCentreService trialCentreService,
                               CharacteristicService characteristicService,
                               OrganisationService organisationService,
                               TrialCentreRepository trialCentreRepository,
                               TrialRepository trialRepository, ContactService contactService,
                               TermlexRestClient gateRestClient) {
        this.conceptRepository = conceptRepository;
        this.conditionService = conditionService;
        this.trialService = trialService;
        this.trialCentreService = trialCentreService;
        this.characteristicService = characteristicService;
        this.organisationService = organisationService;
        this.trialCentreRepository = trialCentreRepository;
        this.trialRepository = trialRepository;
        this.contactService = contactService;
        this.gateRestClient = gateRestClient;
    }

    public ResponseEntity annotateTrial(Trial trial) {

        String text = trial.getTitle();
        ResponseEntity<List> responseEntity = gateRestClient.processText(text);
        log.debug("responseEntity.getBody() = {}", responseEntity.getBody());
        if(responseEntity.getStatusCode() == HttpStatus.OK) {
            log.info("Successfully processed text {}", text);
            List<Map<String, Object>> matches = responseEntity.getBody();
            final Trial finalTrial = trial;
            if (matches != null && !matches.isEmpty()) {
                matches.forEach(match -> {
                    // within each match, we need to get the codes
                    List<Map> codes = (List<Map>) match.get("codes");
                    List<Map> sctCodes = codes.stream().filter(code -> code.get("source").toString().equalsIgnoreCase("SCT")).collect(Collectors.toList());
                    if (!sctCodes.isEmpty()) {
                        sctCodes.forEach(disease -> {
                            String conceptId = String.valueOf(disease.get("id"));
                            boolean existing = trial.getConditions().stream().filter(c -> c.getCode().equalsIgnoreCase(conceptId)).findAny().isPresent();
                            if (!existing) {
                                // create and add condition to trial
                                Condition condition = new Condition().code(conceptId).label(String.valueOf(disease.get("preferredTerm")));
                                condition.setTrialId(finalTrial.getId());
                                condition = conditionService.save(condition);
                                finalTrial.addConditions(condition);
                            }
                        });
                    }
                });

                trialService.save(finalTrial);
            }

        } else {
            log.error("Unable to process text {}", text);
        }

        return responseEntity;
    }

    /**
     * Utility method to associate a condition with a trial if concept with given name/label exists
     *
     * @param trial the trial to link concept to
     * @param label the label of the concept
     */
    public void linkOrAddCondition(Trial trial, final String label) {
        Concept concept = verifyAndGetConcept(label, "condition", null);

        // add condition to trial
        Condition condition = new Condition().code(concept.getId()).label(concept.getLabel());
        condition.setTrialId(trial.getId());
        condition = conditionService.save(condition);
        trial.addConditions(condition);
    }

    /**
     * Utility method that returns a concept with given label if it exists, or creates a new one
     * @param label the label of the concept to check
     * @return the existing or new concept
     */
    public Concept verifyAndGetConcept(String label, String type, String code) {

        Concept concept = null;
        if (label == null || label.isEmpty()) {
            log.error("Concept cannot be created with empty Label");
            throw new IllegalArgumentException("Concept cannot be created with empty Label");
        }

        if (code != null) {
            concept = conceptRepository.findOneByCode(code);
        } else {
            concept = conceptRepository.findOneByLabelAndType(label, type);
        }

        if (concept != null) {
            log.info("Found match for concept label {} ", label);
        } else {
            log.error("No match found for label {}. So will create a new condition.", label);
            // create a new concept and condition to attach
            concept = new Concept(label);
            concept.setType(type);
            if (code != null) {
                concept.setCode(code);
            }
            concept = conceptRepository.save(concept);
        }

        return concept;
    }

    /**
     * Utility method for assigning site id to contacts before saving site and contacts
     * @param trialCentre the site to add contacts to and update
     */
    public void addContactsAndUpdateSite(TrialCentre trialCentre) {
        // loop through each contact and save contacts
        trialCentre.getContacts().forEach(contact -> {
            contact.setTrialCentreId(trialCentre.getId());
            contact = contactService.save(contact);
        });
        // then finally save site
        trialCentreRepository.save(trialCentre);
    }

    public Trial checkAndAddDefaultSite(Trial trial){
        if (trial.getSites() == null || trial.getSites().isEmpty()){
            TrialCentre trialCentre = new TrialCentre();
            Organisation defaultSite = organisationService.findOneByName("University College London Hospitals NHS Foundation Trust");
            trialCentre.fromOrganisation(defaultSite);
            trialCentre.setTrialId(trial.getId());
            trialCentre = trialCentreRepository.save(trialCentre);
            trial.addSite(trialCentre);
            trialRepository.save(trial);
        }
        else {
            trial.getSites().forEach(trialCentre -> {
                if (trialCentre.getTrialId() == null || trialCentre.getTrialId().isEmpty()){
                    trialCentre.setTrialId(trial.getId());
                    trialCentreRepository.save(trialCentre);
                }
            });
        }
        return trial;
    }

    public Trial reattachEligibilities(Trial trial){
        //checking if the trialId for the Eligibilities exists, if not set the value
        Set<Characteristic> characteristics = new HashSet<>();
        characteristics = trial.getEligibilities();
        characteristics.forEach(characteristic -> {
            if (characteristic.getTrialId() == null || characteristic.getTrialId().isEmpty()){
                characteristic.setTrialId(trial.getId());
                characteristicService.save(characteristic);
            }
        });
        return trial;
    }

    public Trial reattachConditions(Trial trial){
        //checking if the trialId for the condition exists, if not set the value
        Set<Condition> conditions = trial.getConditions();
        conditions.forEach(condition -> {
            if (condition.getTrialId() == null || condition.getTrialId().isEmpty()){
                condition.setTrialId(trial.getId());
                conditionService.save(condition);
            }
        });
        return trial;
    }

    public Trial lungTrialsVisit(Trial trial) {
        checkAndAddDefaultSite(trial);
        reattachConditions(trial);
        reattachEligibilities(trial);
//        annotateTrial(trial);
        return trial;
    }

    public Trial visit(Trial trial) {
        Set<Condition> conditions = new HashSet<>();
        Set<String> oldConditionsIds = new HashSet<>();
        // re attach conditions
        for (Condition condition : trial.getConditions()) {
            Concept c = conceptRepository.findOneByIdOrLabel(condition.getCode(), condition.getLabel().trim());
            if(c != null) {
                log.info("Found match for concept id {} or label {} ", condition.getCode(), condition.getLabel());
                Condition con = new Condition().code(c.getId()).label(c.getLabel()).system(condition.getSystem());
                // get associated status
                Concept status = conceptRepository.findOneByIdOrLabel(condition.getStatusCode(), condition.getStatus().trim());
                if(status != null) {
                    log.info("Found match for status id {} or label {} ", condition.getStatusCode(), condition.getStatus());
                    con = con.statusCode(status.getId())
                            .status(status.getLabel());
                } else {
                    throw new IllegalArgumentException("No match found for status code: " + condition.getStatusCode() + " or status label : " + condition.getStatus());
                }
                con.setTrialId(trial.getId());
                con = conditionService.save(con);
                conditions.add(con);
            } else {
                log.error("No match found for id {} or label {} ", condition.getCode(), condition.getLabel());
                throw new IllegalArgumentException("No match found for condition: " + condition.getCode() + " or label : " + condition.getLabel());
            }
            // collect existing id for deletion
            oldConditionsIds.add(condition.getId());
        }
        // now replace all existing conditions with new conditions set
        trial.setConditions(conditions);

        Set<TrialCentre> sites = new HashSet<>();
        Set<String> oldSiteIds = new HashSet<>();
        // re attach sites
        for (TrialCentre site : trial.getSites()) {
            TrialCentre t = trialCentreService.findOne(site.getId());
            if(t == null) {
                log.info("Not found match for site id {}. Will try to reattach ", site.getId());
                // collect existing id for deletion
                oldSiteIds.add(site.getId());
                site.setTrialId(trial.getId());
                site.setId(null);
                site = trialCentreService.save(site);
                sites.add(site);
            } else {
                log.error("Match found for id {}. Assuming site [] is already attached to trial ", site.getId(), site.getName());
            }
        }

        Set<Characteristic> eligibilities = new HashSet<>();
        Set<String> oldEligbilityIds = new HashSet<>();
        // re attach eligibilities
        for (Characteristic characteristic : trial.getEligibilities()) {
            Characteristic c = characteristicService.findOne(characteristic.getId());
            if(c == null) {
                log.info("Not found match for characteristic id {}. Will try to reattach ", characteristic.getId());
                // collect existing id for deletion
                oldEligbilityIds.add(characteristic.getId());
                characteristic.setTrialId(trial.getId());
                characteristic.setId(null);
                characteristic = characteristicService.save(characteristic);
                eligibilities.add(characteristic);
            } else {
                log.error("Match found for id {}. Assuming characteristic is already attached to trial ", characteristic.getId());
            }
        }
        // now replace all existing conditions with new conditions set
        trial.setConditions(conditions);
        trial.setSites(sites);
        trial.setEligibilities(eligibilities);

        // now save trial
        trial = trialService.save(trial);
        log.info("Saved trial {} after updating conditions {}", trial.getId(), conditions);
        // delete old conditions
        oldConditionsIds.forEach(conditionService::delete);
        // delete old sites
        oldSiteIds.forEach(trialCentreService::delete);
        // delete old eligibilities
        oldEligbilityIds.forEach(characteristicService::delete);


        return trial;
    }
}
