package com.trialslink.trialimport;

import com.trialslink.domain.TrialImport;

/**
 * Created by pigiotisk on 14/07/2017.
 */
public interface TrialImporterInterface {
    TrialImport importTrial(TrialImport trialImport);
}
