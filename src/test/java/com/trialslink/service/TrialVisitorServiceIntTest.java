package com.trialslink.service;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Concept;
import com.trialslink.repository.ConceptRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class TrialVisitorServiceIntTest {

    @Autowired
    private ConceptRepository conceptRepository;
    @Autowired
    private TrialVisitorService trialVisitorService;
    private final String CODE = "5c94edf932a7143a40144aef";
    private final String LABEL = "cancer";

    @Before
    public void setUp(){
        conceptRepository.deleteAll();
        assertTrue(conceptRepository.count() == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkNullLabelThrowsAnError(){
        trialVisitorService.verifyAndGetConcept(null,null,CODE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkEmptyLabelThrowsAnError(){
        trialVisitorService.verifyAndGetConcept("",null,CODE);
    }

    @Test
    public void checkIfExistingConceptWithCodeIsReturned(){
        Concept concept = new Concept();
        concept.setLabel(LABEL);
        concept.setCode(CODE);
        conceptRepository.save(concept);
        // we pass a label that is not the same as before, to verify that code is used first
        Concept c = trialVisitorService.verifyAndGetConcept(LABEL+"XXXX",concept.getType(),CODE);
        assertNotNull(c.getCode());
        assertTrue("Returned concept has same code as provided", CODE.equals(c.getCode()));
    }

    @Test
    public void checkIfExistingConceptWithLabelIsReturned(){
        Concept concept = new Concept();
        concept.setLabel(LABEL);
        concept = conceptRepository.save(concept);
        // we pass a null code, to verify the combination of LABEL and type is used for match.
        Concept c = trialVisitorService.verifyAndGetConcept(LABEL,concept.getType(),null);
        assertNotNull(c.getId());
        assertTrue("Returned concept has same code as previously saved", LABEL.equals(c.getLabel()));
    }

    @Test
    public void checkIfConceptIsCreatedWithCodeNull(){
        Concept c = trialVisitorService.verifyAndGetConcept(LABEL,null,null);
        assertNotNull(c);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfConceptIsCreatedWithLabelAndCodeEmpty(){
        Concept c = trialVisitorService.verifyAndGetConcept("","","");
        assertNull(c);
    }
}
