package com.trialslink.service.notifications;

import com.trialslink.domain.SavedSearch;
import com.trialslink.domain.Trial;
import org.apache.lucene.analysis.ar.ArabicAnalyzer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;

/**
 * Created by pigiotisk on 17/08/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class UserNotificationTest {
    @InjectMocks
    private UserNotification userNotification;

    @Mock
    private SavedSearch savedSearch;

    @Mock
    private Trial trial;


    @Test
    public void getSavedSearch() throws Exception {
        assert (userNotification.getSavedSearch() instanceof SavedSearch);
    }

    @Test
    public void setSavedSearch() throws Exception {
        userNotification.setSavedSearch(savedSearch);

        assertEquals(userNotification.getSavedSearch(), savedSearch);
    }

    @Test
    public void getTrialsNull() throws Exception {
        assertNull(userNotification.getTrials());
    }

    @Test
    public void setTrials() throws Exception {
        ArrayList<Trial> trials = new ArrayList<>();
        trials.add(trial);

        userNotification.setTrials(trials);

        assertEquals(userNotification.getTrials(), trials);
    }

}
