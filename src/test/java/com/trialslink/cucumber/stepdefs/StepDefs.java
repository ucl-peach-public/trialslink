package com.trialslink.cucumber.stepdefs;

import com.trialslink.TrialslinkApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = TrialslinkApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
