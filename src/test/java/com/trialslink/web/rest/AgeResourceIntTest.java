package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.Age;
import com.trialslink.repository.AgeRepository;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.trialslink.domain.enumeration.AgeUnit;
/**
 * Test class for the AgeResource REST controller.
 *
 * @see AgeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class AgeResourceIntTest {

    private static final Integer DEFAULT_VALUE = 1;
    private static final Integer UPDATED_VALUE = 2;

    private static final AgeUnit DEFAULT_UNIT = AgeUnit.YEARS;
    private static final AgeUnit UPDATED_UNIT = AgeUnit.MONTHS;

    @Autowired
    private AgeRepository ageRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restAgeMockMvc;

    private Age age;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AgeResource ageResource = new AgeResource(ageRepository);
        this.restAgeMockMvc = MockMvcBuilders.standaloneSetup(ageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Age createEntity() {
        Age age = new Age()
            .value(DEFAULT_VALUE)
            .unit(DEFAULT_UNIT);
        return age;
    }

    @Before
    public void initTest() {
        ageRepository.deleteAll();
        age = createEntity();
    }

    @Test
    public void createAge() throws Exception {
        int databaseSizeBeforeCreate = ageRepository.findAll().size();

        // Create the Age
        restAgeMockMvc.perform(post("/api/ages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(age)))
            .andExpect(status().isCreated());

        // Validate the Age in the database
        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeCreate + 1);
        Age testAge = ageList.get(ageList.size() - 1);
        assertThat(testAge.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testAge.getUnit()).isEqualTo(DEFAULT_UNIT);
    }

    @Test
    public void createAgeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ageRepository.findAll().size();

        // Create the Age with an existing ID
        age.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgeMockMvc.perform(post("/api/ages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(age)))
            .andExpect(status().isBadRequest());

        // Validate the Age in the database
        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = ageRepository.findAll().size();
        // set the field null
        age.setValue(null);

        // Create the Age, which fails.

        restAgeMockMvc.perform(post("/api/ages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(age)))
            .andExpect(status().isBadRequest());

        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkUnitIsRequired() throws Exception {
        int databaseSizeBeforeTest = ageRepository.findAll().size();
        // set the field null
        age.setUnit(null);

        // Create the Age, which fails.

        restAgeMockMvc.perform(post("/api/ages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(age)))
            .andExpect(status().isBadRequest());

        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllAges() throws Exception {
        // Initialize the database
        ageRepository.save(age);

        // Get all the ageList
        restAgeMockMvc.perform(get("/api/ages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(age.getId())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT.toString())));
    }

    @Test
    public void getAge() throws Exception {
        // Initialize the database
        ageRepository.save(age);

        // Get the age
        restAgeMockMvc.perform(get("/api/ages/{id}", age.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(age.getId()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT.toString()));
    }

    @Test
    public void getNonExistingAge() throws Exception {
        // Get the age
        restAgeMockMvc.perform(get("/api/ages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateAge() throws Exception {
        // Initialize the database
        ageRepository.save(age);
        int databaseSizeBeforeUpdate = ageRepository.findAll().size();

        // Update the age
        Age updatedAge = ageRepository.findOne(age.getId());
        updatedAge
            .value(UPDATED_VALUE)
            .unit(UPDATED_UNIT);

        restAgeMockMvc.perform(put("/api/ages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAge)))
            .andExpect(status().isOk());

        // Validate the Age in the database
        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeUpdate);
        Age testAge = ageList.get(ageList.size() - 1);
        assertThat(testAge.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testAge.getUnit()).isEqualTo(UPDATED_UNIT);
    }

    @Test
    public void updateNonExistingAge() throws Exception {
        int databaseSizeBeforeUpdate = ageRepository.findAll().size();

        // Create the Age

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAgeMockMvc.perform(put("/api/ages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(age)))
            .andExpect(status().isCreated());

        // Validate the Age in the database
        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteAge() throws Exception {
        // Initialize the database
        ageRepository.save(age);
        int databaseSizeBeforeDelete = ageRepository.findAll().size();

        // Get the age
        restAgeMockMvc.perform(delete("/api/ages/{id}", age.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Age> ageList = ageRepository.findAll();
        assertThat(ageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Age.class);
        Age age1 = new Age();
        age1.setId("id1");
        Age age2 = new Age();
        age2.setId(age1.getId());
        assertThat(age1).isEqualTo(age2);
        age2.setId("id2");
        assertThat(age1).isNotEqualTo(age2);
        age1.setId(null);
        assertThat(age1).isNotEqualTo(age2);
    }
}
