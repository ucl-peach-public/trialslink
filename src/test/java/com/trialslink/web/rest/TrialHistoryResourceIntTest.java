package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.TrialHistory;
import com.trialslink.repository.TrialHistoryRepository;
import com.trialslink.service.TrialHistoryService;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TrialHistoryResource REST controller.
 *
 * @see TrialHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class TrialHistoryResourceIntTest {

    @Autowired
    private TrialHistoryRepository trialHistoryRepository;

    @Autowired
    private TrialHistoryService trialHistoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTrialHistoryMockMvc;

    private TrialHistory trialHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TrialHistoryResource trialHistoryResource = new TrialHistoryResource(trialHistoryService);
        this.restTrialHistoryMockMvc = MockMvcBuilders.standaloneSetup(trialHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrialHistory createEntity() {
        TrialHistory trialHistory = new TrialHistory();
        return trialHistory;
    }

    @Before
    public void initTest() {
        trialHistoryRepository.deleteAll();
        trialHistory = createEntity();
    }

    /*@Test
    public void createTrialHistory() throws Exception {
        int databaseSizeBeforeCreate = trialHistoryRepository.findAll().size();

        // Create the TrialHistory
        restTrialHistoryMockMvc.perform(post("/api/trial-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialHistory)))
            .andExpect(status().isCreated());

        // Validate the TrialHistory in the database
        List<TrialHistory> trialHistoryList = trialHistoryRepository.findAll();
        assertThat(trialHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        TrialHistory testTrialHistory = trialHistoryList.get(trialHistoryList.size() - 1);
    }*/

    /*@Test
    public void createTrialHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trialHistoryRepository.findAll().size();

        // Create the TrialHistory with an existing ID
        trialHistory.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrialHistoryMockMvc.perform(post("/api/trial-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialHistory)))
            .andExpect(status().isBadRequest());

        // Validate the TrialHistory in the database
        List<TrialHistory> trialHistoryList = trialHistoryRepository.findAll();
        assertThat(trialHistoryList).hasSize(databaseSizeBeforeCreate);
    }*/

    /*@Test
    public void getAllTrialHistories() throws Exception {
        // Initialize the database
        trialHistoryRepository.save(trialHistory);

        // Get all the trialHistoryList
        restTrialHistoryMockMvc.perform(get("/api/trial-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trialHistory.getId())));
    }*/

    @Test
    public void getTrialHistory() throws Exception {
        // Initialize the database
        trialHistoryRepository.save(trialHistory);

        // Get the trialHistory
        restTrialHistoryMockMvc.perform(get("/api/trial-histories/{id}", trialHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trialHistory.getId()));
    }

    @Test
    public void getNonExistingTrialHistory() throws Exception {
        // Get the trialHistory
        restTrialHistoryMockMvc.perform(get("/api/trial-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    /*@Test
    public void updateTrialHistory() throws Exception {
        // Initialize the database
        trialHistoryService.save(trialHistory);

        int databaseSizeBeforeUpdate = trialHistoryRepository.findAll().size();

        // Update the trialHistory
        TrialHistory updatedTrialHistory = trialHistoryRepository.findOne(trialHistory.getId());

        restTrialHistoryMockMvc.perform(put("/api/trial-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrialHistory)))
            .andExpect(status().isOk());

        // Validate the TrialHistory in the database
        List<TrialHistory> trialHistoryList = trialHistoryRepository.findAll();
        assertThat(trialHistoryList).hasSize(databaseSizeBeforeUpdate);
        TrialHistory testTrialHistory = trialHistoryList.get(trialHistoryList.size() - 1);
    }*/

    /*@Test
    public void updateNonExistingTrialHistory() throws Exception {
        int databaseSizeBeforeUpdate = trialHistoryRepository.findAll().size();

        // Create the TrialHistory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTrialHistoryMockMvc.perform(put("/api/trial-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialHistory)))
            .andExpect(status().isCreated());

        // Validate the TrialHistory in the database
        List<TrialHistory> trialHistoryList = trialHistoryRepository.findAll();
        assertThat(trialHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }*/

    /*@Test
    public void deleteTrialHistory() throws Exception {
        // Initialize the database
        trialHistoryService.save(trialHistory);

        int databaseSizeBeforeDelete = trialHistoryRepository.findAll().size();

        // Get the trialHistory
        restTrialHistoryMockMvc.perform(delete("/api/trial-histories/{id}", trialHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TrialHistory> trialHistoryList = trialHistoryRepository.findAll();
        assertThat(trialHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }*/

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrialHistory.class);
        TrialHistory trialHistory1 = new TrialHistory();
        trialHistory1.setId("id1");
        TrialHistory trialHistory2 = new TrialHistory();
        trialHistory2.setId(trialHistory1.getId());
        assertThat(trialHistory1).isEqualTo(trialHistory2);
        trialHistory2.setId("id2");
        assertThat(trialHistory1).isNotEqualTo(trialHistory2);
        trialHistory1.setId(null);
        assertThat(trialHistory1).isNotEqualTo(trialHistory2);
    }
}
