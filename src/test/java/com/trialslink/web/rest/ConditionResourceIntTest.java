package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Condition;
import com.trialslink.service.ConditionService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConditionResource REST controller.
 *
 * @see ConditionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class ConditionResourceIntTest {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_SYSTEM = "AAAAAAAAAA";
    private static final String UPDATED_SYSTEM = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_CODE = "BBBBBBBBBB";

    @Autowired
    private ConditionService conditionRepository;
    @Autowired
    private TrialService trialService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restConditionMockMvc;

    private Condition condition;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConditionResource conditionResource = new ConditionResource(conditionRepository, trialService);
        this.restConditionMockMvc = MockMvcBuilders.standaloneSetup(conditionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Condition createEntity() {
        Condition condition = new Condition()
            .label(DEFAULT_LABEL)
            .system(DEFAULT_SYSTEM)
            .code(DEFAULT_CODE)
            .status(DEFAULT_STATUS)
            .statusCode(DEFAULT_STATUS_CODE);
        return condition;
    }

    @Before
    public void initTest() {
        conditionRepository.deleteAll();
        condition = createEntity();
    }

    @Test
    public void createCondition() throws Exception {
        int databaseSizeBeforeCreate = conditionRepository.findAllAsList().size();

        // Create the Condition
        restConditionMockMvc.perform(post("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(condition)))
            .andExpect(status().isCreated());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAllAsList();
        assertThat(conditionList).hasSize(databaseSizeBeforeCreate + 1);
        Condition testCondition = conditionList.get(conditionList.size() - 1);
        assertThat(testCondition.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testCondition.getSystem()).isEqualTo(DEFAULT_SYSTEM);
        assertThat(testCondition.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCondition.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCondition.getStatusCode()).isEqualTo(DEFAULT_STATUS_CODE);
    }

    @Test
    public void createConditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = conditionRepository.findAllAsList().size();

        // Create the Condition with an existing ID
        condition.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restConditionMockMvc.perform(post("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(condition)))
            .andExpect(status().isBadRequest());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAllAsList();
        assertThat(conditionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = conditionRepository.findAllAsList().size();
        // set the field null
        condition.setLabel(null);

        // Create the Condition, which fails.

        restConditionMockMvc.perform(post("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(condition)))
            .andExpect(status().isBadRequest());

        List<Condition> conditionList = conditionRepository.findAllAsList();
        assertThat(conditionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllConditions() throws Exception {
        // Initialize the database
        conditionRepository.save(condition);

        // Get all the conditionList
        restConditionMockMvc.perform(get("/api/conditions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(condition.getId())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())))
            .andExpect(jsonPath("$.[*].system").value(hasItem(DEFAULT_SYSTEM.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusCode").value(hasItem(DEFAULT_STATUS_CODE.toString())));
    }

    @Test
    public void getCondition() throws Exception {
        // Initialize the database
        conditionRepository.save(condition);

        // Get the condition
        restConditionMockMvc.perform(get("/api/conditions/{id}", condition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(condition.getId()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()))
            .andExpect(jsonPath("$.system").value(DEFAULT_SYSTEM.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.statusCode").value(DEFAULT_STATUS_CODE.toString()));
    }

    @Test
    public void getNonExistingCondition() throws Exception {
        // Get the condition
        restConditionMockMvc.perform(get("/api/conditions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCondition() throws Exception {
        // Initialize the database
        conditionRepository.save(condition);
        int databaseSizeBeforeUpdate = conditionRepository.findAllAsList().size();

        // Update the condition
        Condition updatedCondition = conditionRepository.findOne(condition.getId());
        updatedCondition
            .label(UPDATED_LABEL)
            .system(UPDATED_SYSTEM)
            .code(UPDATED_CODE)
            .status(UPDATED_STATUS)
            .statusCode(UPDATED_STATUS_CODE);

        restConditionMockMvc.perform(put("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCondition)))
            .andExpect(status().isOk());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAllAsList();
        assertThat(conditionList).hasSize(databaseSizeBeforeUpdate);
        Condition testCondition = conditionList.get(conditionList.size() - 1);
        assertThat(testCondition.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testCondition.getSystem()).isEqualTo(UPDATED_SYSTEM);
        assertThat(testCondition.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCondition.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCondition.getStatusCode()).isEqualTo(UPDATED_STATUS_CODE);
    }

    @Test
    public void updateNonExistingCondition() throws Exception {
        int databaseSizeBeforeUpdate = conditionRepository.findAllAsList().size();

        // Create the Condition

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConditionMockMvc.perform(put("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(condition)))
            .andExpect(status().isCreated());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAllAsList();
        assertThat(conditionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteCondition() throws Exception {
        // Initialize the database
        conditionRepository.save(condition);
        int databaseSizeBeforeDelete = conditionRepository.findAllAsList().size();

        // Get the condition
        restConditionMockMvc.perform(delete("/api/conditions/{id}", condition.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Condition> conditionList = conditionRepository.findAllAsList();
        assertThat(conditionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Condition.class);
        Condition condition1 = new Condition();
        condition1.setId("id1");
        Condition condition2 = new Condition();
        condition2.setId(condition1.getId());
        assertThat(condition1).isEqualTo(condition2);
        condition2.setId("id2");
        assertThat(condition1).isNotEqualTo(condition2);
        condition1.setId(null);
        assertThat(condition1).isNotEqualTo(condition2);
    }
}
