package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.SecondaryId;
import com.trialslink.repository.SecondaryIdRepository;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SecondaryIdResource REST controller.
 *
 * @see SecondaryIdResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class SecondaryIdResourceIntTest {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private SecondaryIdRepository secondaryIdRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restSecondaryIdMockMvc;

    private SecondaryId secondaryId;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SecondaryIdResource secondaryIdResource = new SecondaryIdResource(secondaryIdRepository);
        this.restSecondaryIdMockMvc = MockMvcBuilders.standaloneSetup(secondaryIdResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SecondaryId createEntity() {
        SecondaryId secondaryId = new SecondaryId()
            .type(DEFAULT_TYPE)
            .value(DEFAULT_VALUE);
        return secondaryId;
    }

    @Before
    public void initTest() {
        secondaryIdRepository.deleteAll();
        secondaryId = createEntity();
    }

    @Test
    public void createSecondaryId() throws Exception {
        int databaseSizeBeforeCreate = secondaryIdRepository.findAll().size();

        // Create the SecondaryId
        restSecondaryIdMockMvc.perform(post("/api/secondary-ids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondaryId)))
            .andExpect(status().isCreated());

        // Validate the SecondaryId in the database
        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeCreate + 1);
        SecondaryId testSecondaryId = secondaryIdList.get(secondaryIdList.size() - 1);
        assertThat(testSecondaryId.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testSecondaryId.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    public void createSecondaryIdWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = secondaryIdRepository.findAll().size();

        // Create the SecondaryId with an existing ID
        secondaryId.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restSecondaryIdMockMvc.perform(post("/api/secondary-ids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondaryId)))
            .andExpect(status().isBadRequest());

        // Validate the SecondaryId in the database
        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = secondaryIdRepository.findAll().size();
        // set the field null
        secondaryId.setType(null);

        // Create the SecondaryId, which fails.

        restSecondaryIdMockMvc.perform(post("/api/secondary-ids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondaryId)))
            .andExpect(status().isBadRequest());

        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = secondaryIdRepository.findAll().size();
        // set the field null
        secondaryId.setValue(null);

        // Create the SecondaryId, which fails.

        restSecondaryIdMockMvc.perform(post("/api/secondary-ids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondaryId)))
            .andExpect(status().isBadRequest());

        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllSecondaryIds() throws Exception {
        // Initialize the database
        secondaryIdRepository.save(secondaryId);

        // Get all the secondaryIdList
        restSecondaryIdMockMvc.perform(get("/api/secondary-ids?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(secondaryId.getId())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    public void getSecondaryId() throws Exception {
        // Initialize the database
        secondaryIdRepository.save(secondaryId);

        // Get the secondaryId
        restSecondaryIdMockMvc.perform(get("/api/secondary-ids/{id}", secondaryId.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(secondaryId.getId()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    public void getNonExistingSecondaryId() throws Exception {
        // Get the secondaryId
        restSecondaryIdMockMvc.perform(get("/api/secondary-ids/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSecondaryId() throws Exception {
        // Initialize the database
        secondaryIdRepository.save(secondaryId);
        int databaseSizeBeforeUpdate = secondaryIdRepository.findAll().size();

        // Update the secondaryId
        SecondaryId updatedSecondaryId = secondaryIdRepository.findOne(secondaryId.getId());
        updatedSecondaryId
            .type(UPDATED_TYPE)
            .value(UPDATED_VALUE);

        restSecondaryIdMockMvc.perform(put("/api/secondary-ids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSecondaryId)))
            .andExpect(status().isOk());

        // Validate the SecondaryId in the database
        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeUpdate);
        SecondaryId testSecondaryId = secondaryIdList.get(secondaryIdList.size() - 1);
        assertThat(testSecondaryId.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testSecondaryId.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    public void updateNonExistingSecondaryId() throws Exception {
        int databaseSizeBeforeUpdate = secondaryIdRepository.findAll().size();

        // Create the SecondaryId

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSecondaryIdMockMvc.perform(put("/api/secondary-ids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondaryId)))
            .andExpect(status().isCreated());

        // Validate the SecondaryId in the database
        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteSecondaryId() throws Exception {
        // Initialize the database
        secondaryIdRepository.save(secondaryId);
        int databaseSizeBeforeDelete = secondaryIdRepository.findAll().size();

        // Get the secondaryId
        restSecondaryIdMockMvc.perform(delete("/api/secondary-ids/{id}", secondaryId.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SecondaryId> secondaryIdList = secondaryIdRepository.findAll();
        assertThat(secondaryIdList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SecondaryId.class);
        SecondaryId secondaryId1 = new SecondaryId();
        secondaryId1.setId("id1");
        SecondaryId secondaryId2 = new SecondaryId();
        secondaryId2.setId(secondaryId1.getId());
        assertThat(secondaryId1).isEqualTo(secondaryId2);
        secondaryId2.setId("id2");
        assertThat(secondaryId1).isNotEqualTo(secondaryId2);
        secondaryId1.setId(null);
        assertThat(secondaryId1).isNotEqualTo(secondaryId2);
    }
}
