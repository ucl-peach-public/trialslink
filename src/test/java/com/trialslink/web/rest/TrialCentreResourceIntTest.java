package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Day;
import com.trialslink.domain.TrialCentre;
import com.trialslink.repository.TrialCentreRepository;
import com.trialslink.service.ContactService;
import com.trialslink.service.TrialCentreService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static com.trialslink.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TrialCentreResource REST controller.
 *
 * @see TrialCentreResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class TrialCentreResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ALL_DAY = false;
    private static final Boolean UPDATED_ALL_DAY = true;

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private Set<Day> DEFAULT_DAYS = new HashSet<>();
    private Set<Day> UPDATED_DAYS = new HashSet<>();

    @Autowired
    private TrialCentreRepository trialCentreRepository;

    @Autowired
    private TrialCentreService trialCentreService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private TrialService trialService;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTrialCentreMockMvc;

    private TrialCentre trialCentre;

    @Before
    public void setup() {
        DEFAULT_DAYS = new HashSet<>();
        DEFAULT_DAYS.add(new Day("AAAAAAAAAA"));

        UPDATED_DAYS = new HashSet<>();
        UPDATED_DAYS.add(new Day("BBBBBBBBBB"));
        MockitoAnnotations.initMocks(this);
        final TrialCentreResource trialCentreResource = new TrialCentreResource(trialCentreService, contactService, trialService);
        this.restTrialCentreMockMvc = MockMvcBuilders.standaloneSetup(trialCentreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public TrialCentre createEntity() {
        TrialCentre trialCentre = new TrialCentre()
            .allDay(DEFAULT_ALL_DAY)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .days(DEFAULT_DAYS);
        trialCentre.setName(DEFAULT_NAME);
        return trialCentre;
    }

    @Before
    public void initTest() {
        trialCentreRepository.deleteAll();
        trialCentre = createEntity();
    }

    @Test
    public void createTrialCentre() throws Exception {
        int databaseSizeBeforeCreate = trialCentreRepository.findAll().size();

        // Create the TrialCentre
        restTrialCentreMockMvc.perform(post("/api/trial-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialCentre)))
            .andExpect(status().isCreated());

        // Validate the TrialCentre in the database
        List<TrialCentre> trialCentreList = trialCentreRepository.findAll();
        assertThat(trialCentreList).hasSize(databaseSizeBeforeCreate + 1);
        TrialCentre testTrialCentre = trialCentreList.get(trialCentreList.size() - 1);
        assertThat(testTrialCentre.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTrialCentre.isAllDay()).isEqualTo(DEFAULT_ALL_DAY);
        assertThat(testTrialCentre.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testTrialCentre.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testTrialCentre.getDays()).isEqualTo(DEFAULT_DAYS);
    }

    @Test
    public void createTrialCentreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trialCentreRepository.findAll().size();

        // Create the TrialCentre with an existing ID
        trialCentre.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrialCentreMockMvc.perform(post("/api/trial-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialCentre)))
            .andExpect(status().isBadRequest());

        // Validate the TrialCentre in the database
        List<TrialCentre> trialCentreList = trialCentreRepository.findAll();
        assertThat(trialCentreList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialCentreRepository.findAll().size();
        // set the field null
        trialCentre.setName(null);

        // Create the TrialCentre, which fails.

        restTrialCentreMockMvc.perform(post("/api/trial-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialCentre)))
            .andExpect(status().isBadRequest());

        List<TrialCentre> trialCentreList = trialCentreRepository.findAll();
        assertThat(trialCentreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllTrialCentres() throws Exception {
        // Initialize the database
        trialCentreRepository.save(trialCentre);

        // Get all the trialCentreList
        restTrialCentreMockMvc.perform(get("/api/trial-centres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trialCentre.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].allDay").value(hasItem(DEFAULT_ALL_DAY.booleanValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(sameInstant(DEFAULT_END_DATE))))
            .andExpect(jsonPath("$.[*].days").value(hasItem(DEFAULT_DAYS.toString())));
    }

    @Test
    public void getTrialCentre() throws Exception {
        // Initialize the database
        trialCentreRepository.save(trialCentre);

        // Get the trialCentre
        restTrialCentreMockMvc.perform(get("/api/trial-centres/{id}", trialCentre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trialCentre.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.allDay").value(DEFAULT_ALL_DAY.booleanValue()))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.endDate").value(sameInstant(DEFAULT_END_DATE)))
            .andExpect(jsonPath("$.days").value(DEFAULT_DAYS.toString()));
    }

    @Test
    public void getNonExistingTrialCentre() throws Exception {
        // Get the trialCentre
        restTrialCentreMockMvc.perform(get("/api/trial-centres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateTrialCentre() throws Exception {
        // Initialize the database
        trialCentreService.save(trialCentre);

        int databaseSizeBeforeUpdate = trialCentreRepository.findAll().size();

        // Update the trialCentre
        TrialCentre updatedTrialCentre = trialCentreRepository.findOne(trialCentre.getId());
        updatedTrialCentre
            .allDay(UPDATED_ALL_DAY)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .days(UPDATED_DAYS);
        updatedTrialCentre.setName(UPDATED_NAME);

        restTrialCentreMockMvc.perform(put("/api/trial-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrialCentre)))
            .andExpect(status().isOk());

        // Validate the TrialCentre in the database
        List<TrialCentre> trialCentreList = trialCentreRepository.findAll();
        assertThat(trialCentreList).hasSize(databaseSizeBeforeUpdate);
        TrialCentre testTrialCentre = trialCentreList.get(trialCentreList.size() - 1);
        assertThat(testTrialCentre.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTrialCentre.isAllDay()).isEqualTo(UPDATED_ALL_DAY);
        assertThat(testTrialCentre.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testTrialCentre.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testTrialCentre.getDays()).isEqualTo(UPDATED_DAYS);
    }

    @Test
    public void updateNonExistingTrialCentre() throws Exception {
        int databaseSizeBeforeUpdate = trialCentreRepository.findAll().size();

        // Create the TrialCentre

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTrialCentreMockMvc.perform(put("/api/trial-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialCentre)))
            .andExpect(status().isCreated());

        // Validate the TrialCentre in the database
        List<TrialCentre> trialCentreList = trialCentreRepository.findAll();
        assertThat(trialCentreList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteTrialCentre() throws Exception {
        // Initialize the database
        trialCentreService.save(trialCentre);

        int databaseSizeBeforeDelete = trialCentreRepository.findAll().size();

        // Get the trialCentre
        restTrialCentreMockMvc.perform(delete("/api/trial-centres/{id}", trialCentre.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TrialCentre> trialCentreList = trialCentreRepository.findAll();
        assertThat(trialCentreList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrialCentre.class);
        TrialCentre trialCentre1 = new TrialCentre();
        trialCentre1.setId("id1");
        TrialCentre trialCentre2 = new TrialCentre();
        trialCentre2.setId(trialCentre1.getId());
        assertThat(trialCentre1).isEqualTo(trialCentre2);
        trialCentre2.setId("id2");
        assertThat(trialCentre1).isNotEqualTo(trialCentre2);
        trialCentre1.setId(null);
        assertThat(trialCentre1).isNotEqualTo(trialCentre2);
    }
}
