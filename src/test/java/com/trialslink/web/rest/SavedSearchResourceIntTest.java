package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.SavedSearch;
import com.trialslink.repository.SavedSearchRepository;
import com.trialslink.service.SavedSearchService;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.trialslink.web.rest.TestUtil.sameInstant;
import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SavedSearchResource REST controller.
 *
 * @see SavedSearchResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class SavedSearchResourceIntTest {

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_HASH = "AAAAAAAAAA";
    private static final String UPDATED_HASH = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_SHOULD_NOTIFY = false;
    private static final Boolean UPDATED_SHOULD_NOTIFY = true;

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    @Autowired
    private SavedSearchRepository savedSearchRepository;

    @Autowired
    private SavedSearchService savedSearchService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restSavedSearchMockMvc;

    private SavedSearch savedSearch;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SavedSearchResource savedSearchResource = new SavedSearchResource(savedSearchService);
        this.restSavedSearchMockMvc = MockMvcBuilders.standaloneSetup(savedSearchResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SavedSearch createEntity() {
        SavedSearch savedSearch = new SavedSearch()
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .url(DEFAULT_URL)
            .hash(DEFAULT_HASH)
            .createdBy(DEFAULT_CREATED_BY)
            .shouldNotify(DEFAULT_SHOULD_NOTIFY)
            .title(DEFAULT_TITLE);
        return savedSearch;
    }

    @Before
    public void initTest() {
        savedSearchRepository.deleteAll();
        savedSearch = createEntity();
    }

    @Test
    public void createSavedSearch() throws Exception {
        int databaseSizeBeforeCreate = savedSearchRepository.findAll().size();

        // Create the SavedSearch
        restSavedSearchMockMvc.perform(post("/api/saved-searches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savedSearch)))
            .andExpect(status().isCreated());

        // Validate the SavedSearch in the database
        List<SavedSearch> savedSearchList = savedSearchRepository.findAll();
        assertThat(savedSearchList).hasSize(databaseSizeBeforeCreate + 1);
        SavedSearch testSavedSearch = savedSearchList.get(savedSearchList.size() - 1);
        assertThat(testSavedSearch.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testSavedSearch.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testSavedSearch.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testSavedSearch.getHash()).isEqualTo(DEFAULT_HASH);
        assertThat(testSavedSearch.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSavedSearch.isShouldNotify()).isEqualTo(DEFAULT_SHOULD_NOTIFY);
        assertThat(testSavedSearch.getTitle()).isEqualTo(DEFAULT_TITLE);
    }

    @Test
    public void createSavedSearchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = savedSearchRepository.findAll().size();

        // Create the SavedSearch with an existing ID
        savedSearch.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restSavedSearchMockMvc.perform(post("/api/saved-searches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savedSearch)))
            .andExpect(status().isBadRequest());

        // Validate the SavedSearch in the database
        List<SavedSearch> savedSearchList = savedSearchRepository.findAll();
        assertThat(savedSearchList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = savedSearchRepository.findAll().size();
        // set the field null
        savedSearch.setTitle(null);

        // Create the SavedSearch, which fails.

        restSavedSearchMockMvc.perform(post("/api/saved-searches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savedSearch)))
            .andExpect(status().isBadRequest());

        List<SavedSearch> savedSearchList = savedSearchRepository.findAll();
        assertThat(savedSearchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllSavedSearches() throws Exception {
        // Initialize the database
        savedSearchRepository.save(savedSearch);

        // Get all the savedSearchList
        restSavedSearchMockMvc.perform(get("/api/saved-searches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(savedSearch.getId())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].hash").value(hasItem(DEFAULT_HASH.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].shouldNotify").value(hasItem(DEFAULT_SHOULD_NOTIFY.booleanValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())));
    }

    @Test
    public void getSavedSearch() throws Exception {
        // Initialize the database
        savedSearchRepository.save(savedSearch);

        // Get the savedSearch
        restSavedSearchMockMvc.perform(get("/api/saved-searches/{id}", savedSearch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(savedSearch.getId()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.hash").value(DEFAULT_HASH.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.shouldNotify").value(DEFAULT_SHOULD_NOTIFY.booleanValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()));
    }

    @Test
    public void getNonExistingSavedSearch() throws Exception {
        // Get the savedSearch
        restSavedSearchMockMvc.perform(get("/api/saved-searches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSavedSearch() throws Exception {
        // Initialize the database
        savedSearchService.save(savedSearch);

        int databaseSizeBeforeUpdate = savedSearchRepository.findAll().size();

        // Update the savedSearch
        SavedSearch updatedSavedSearch = savedSearchRepository.findOne(savedSearch.getId());
        updatedSavedSearch
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .url(UPDATED_URL)
            .hash(UPDATED_HASH)
            .createdBy(UPDATED_CREATED_BY)
            .shouldNotify(UPDATED_SHOULD_NOTIFY)
            .title(UPDATED_TITLE);

        restSavedSearchMockMvc.perform(put("/api/saved-searches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSavedSearch)))
            .andExpect(status().isOk());

        // Validate the SavedSearch in the database
        List<SavedSearch> savedSearchList = savedSearchRepository.findAll();
        assertThat(savedSearchList).hasSize(databaseSizeBeforeUpdate);
        SavedSearch testSavedSearch = savedSearchList.get(savedSearchList.size() - 1);
        assertThat(testSavedSearch.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testSavedSearch.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testSavedSearch.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testSavedSearch.getHash()).isEqualTo(UPDATED_HASH);
        assertThat(testSavedSearch.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSavedSearch.isShouldNotify()).isEqualTo(UPDATED_SHOULD_NOTIFY);
        assertThat(testSavedSearch.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    public void updateNonExistingSavedSearch() throws Exception {
        int databaseSizeBeforeUpdate = savedSearchRepository.findAll().size();

        // Create the SavedSearch

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSavedSearchMockMvc.perform(put("/api/saved-searches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savedSearch)))
            .andExpect(status().isCreated());

        // Validate the SavedSearch in the database
        List<SavedSearch> savedSearchList = savedSearchRepository.findAll();
        assertThat(savedSearchList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteSavedSearch() throws Exception {
        // Initialize the database
        savedSearchService.save(savedSearch);

        int databaseSizeBeforeDelete = savedSearchRepository.findAll().size();

        // Get the savedSearch
        restSavedSearchMockMvc.perform(delete("/api/saved-searches/{id}", savedSearch.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SavedSearch> savedSearchList = savedSearchRepository.findAll();
        assertThat(savedSearchList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SavedSearch.class);
        SavedSearch savedSearch1 = new SavedSearch();
        savedSearch1.setId("id1");
        SavedSearch savedSearch2 = new SavedSearch();
        savedSearch2.setId(savedSearch1.getId());
        assertThat(savedSearch1).isEqualTo(savedSearch2);
        savedSearch2.setId("id2");
        assertThat(savedSearch1).isNotEqualTo(savedSearch2);
        savedSearch1.setId(null);
        assertThat(savedSearch1).isNotEqualTo(savedSearch2);
    }
}
