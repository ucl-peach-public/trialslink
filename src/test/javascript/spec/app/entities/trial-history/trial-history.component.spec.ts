/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialHistoryComponent } from '../../../../../../main/webapp/app/entities/trial-history/trial-history.component';
import { TrialHistoryService } from '../../../../../../main/webapp/app/entities/trial-history/trial-history.service';
import { TrialHistory } from '../../../../../../main/webapp/app/entities/trial-history/trial-history.model';

describe('Component Tests', () => {

    describe('TrialHistory Management Component', () => {
        let comp: TrialHistoryComponent;
        let fixture: ComponentFixture<TrialHistoryComponent>;
        let service: TrialHistoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialHistoryComponent],
                providers: [
                    TrialHistoryService
                ]
            })
            .overrideTemplate(TrialHistoryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialHistoryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialHistoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new TrialHistory('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.trialHistories[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
