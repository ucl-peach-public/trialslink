/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { OutcomeMeasureDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure-delete-dialog.component';
import { OutcomeMeasureService } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.service';

describe('Component Tests', () => {

    describe('OutcomeMeasure Management Delete Component', () => {
        let comp: OutcomeMeasureDeleteDialogComponent;
        let fixture: ComponentFixture<OutcomeMeasureDeleteDialogComponent>;
        let service: OutcomeMeasureService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [OutcomeMeasureDeleteDialogComponent],
                providers: [
                    OutcomeMeasureService
                ]
            })
            .overrideTemplate(OutcomeMeasureDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OutcomeMeasureDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OutcomeMeasureService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
