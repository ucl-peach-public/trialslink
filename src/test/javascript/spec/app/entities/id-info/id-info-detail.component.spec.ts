/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { IdInfoDetailComponent } from '../../../../../../main/webapp/app/entities/id-info/id-info-detail.component';
import { IdInfoService } from '../../../../../../main/webapp/app/entities/id-info/id-info.service';
import { IdInfo } from '../../../../../../main/webapp/app/entities/id-info/id-info.model';

describe('Component Tests', () => {

    describe('IdInfo Management Detail Component', () => {
        let comp: IdInfoDetailComponent;
        let fixture: ComponentFixture<IdInfoDetailComponent>;
        let service: IdInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [IdInfoDetailComponent],
                providers: [
                    IdInfoService
                ]
            })
            .overrideTemplate(IdInfoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdInfoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdInfoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new IdInfo('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.idInfo).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
