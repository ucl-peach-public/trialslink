/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { OrganisationDetailComponent } from '../../../../../../main/webapp/app/entities/organisation/organisation-detail.component';
import { OrganisationService } from '../../../../../../main/webapp/app/entities/organisation/organisation.service';
import { Organisation } from '../../../../../../main/webapp/app/entities/organisation/organisation.model';

describe('Component Tests', () => {

    describe('Organisation Management Detail Component', () => {
        let comp: OrganisationDetailComponent;
        let fixture: ComponentFixture<OrganisationDetailComponent>;
        let service: OrganisationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [OrganisationDetailComponent],
                providers: [
                    OrganisationService
                ]
            })
            .overrideTemplate(OrganisationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OrganisationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrganisationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Organisation('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.organisation).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
