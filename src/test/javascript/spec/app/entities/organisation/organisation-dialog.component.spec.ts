/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { OrganisationDialogComponent } from '../../../../../../main/webapp/app/entities/organisation/organisation-dialog.component';
import { OrganisationService } from '../../../../../../main/webapp/app/entities/organisation/organisation.service';
import { Organisation } from '../../../../../../main/webapp/app/entities/organisation/organisation.model';

describe('Component Tests', () => {

    describe('Organisation Management Dialog Component', () => {
        let comp: OrganisationDialogComponent;
        let fixture: ComponentFixture<OrganisationDialogComponent>;
        let service: OrganisationService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [OrganisationDialogComponent],
                providers: [
                    OrganisationService
                ]
            })
            .overrideTemplate(OrganisationDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OrganisationDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrganisationService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Organisation('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.organisation = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'organisationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Organisation();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.organisation = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'organisationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
