/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialCentreDialogComponent } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre-dialog.component';
import { TrialCentreService } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.service';
import { TrialCentre } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.model';

describe('Component Tests', () => {

    describe('TrialCentre Management Dialog Component', () => {
        let comp: TrialCentreDialogComponent;
        let fixture: ComponentFixture<TrialCentreDialogComponent>;
        let service: TrialCentreService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialCentreDialogComponent],
                providers: [
                    TrialCentreService
                ]
            })
            .overrideTemplate(TrialCentreDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialCentreDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialCentreService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TrialCentre('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.trialCentre = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'trialCentreListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TrialCentre();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.trialCentre = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'trialCentreListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
