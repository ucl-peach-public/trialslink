/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialCentreDetailComponent } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre-detail.component';
import { TrialCentreService } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.service';
import { TrialCentre } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.model';

describe('Component Tests', () => {

    describe('TrialCentre Management Detail Component', () => {
        let comp: TrialCentreDetailComponent;
        let fixture: ComponentFixture<TrialCentreDetailComponent>;
        let service: TrialCentreService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialCentreDetailComponent],
                providers: [
                    TrialCentreService
                ]
            })
            .overrideTemplate(TrialCentreDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialCentreDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialCentreService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new TrialCentre('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.trialCentre).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
