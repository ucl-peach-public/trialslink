/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { AgeComponent } from '../../../../../../main/webapp/app/entities/age/age.component';
import { AgeService } from '../../../../../../main/webapp/app/entities/age/age.service';
import { Age } from '../../../../../../main/webapp/app/entities/age/age.model';

describe('Component Tests', () => {

    describe('Age Management Component', () => {
        let comp: AgeComponent;
        let fixture: ComponentFixture<AgeComponent>;
        let service: AgeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [AgeComponent],
                providers: [
                    AgeService
                ]
            })
            .overrideTemplate(AgeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AgeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Age('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.ages[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
