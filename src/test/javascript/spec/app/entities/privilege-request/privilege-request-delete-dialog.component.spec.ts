/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { PrivilegeRequestDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request-delete-dialog.component';
import { PrivilegeRequestService } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.service';

describe('Component Tests', () => {

    describe('PrivilegeRequest Management Delete Component', () => {
        let comp: PrivilegeRequestDeleteDialogComponent;
        let fixture: ComponentFixture<PrivilegeRequestDeleteDialogComponent>;
        let service: PrivilegeRequestService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PrivilegeRequestDeleteDialogComponent],
                providers: [
                    PrivilegeRequestService
                ]
            })
            .overrideTemplate(PrivilegeRequestDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PrivilegeRequestDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PrivilegeRequestService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
