/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { SavedSearchComponent } from '../../../../../../main/webapp/app/entities/saved-search/saved-search.component';
import { SavedSearchService } from '../../../../../../main/webapp/app/entities/saved-search/saved-search.service';
import { SavedSearch } from '../../../../../../main/webapp/app/entities/saved-search/saved-search.model';

describe('Component Tests', () => {

    describe('SavedSearch Management Component', () => {
        let comp: SavedSearchComponent;
        let fixture: ComponentFixture<SavedSearchComponent>;
        let service: SavedSearchService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [SavedSearchComponent],
                providers: [
                    SavedSearchService
                ]
            })
            .overrideTemplate(SavedSearchComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SavedSearchComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SavedSearchService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new SavedSearch('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.savedSearches[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
