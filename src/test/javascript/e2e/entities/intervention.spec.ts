import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Intervention e2e test', () => {

    let navBarPage: NavBarPage;
    let interventionDialogPage: InterventionDialogPage;
    let interventionComponentsPage: InterventionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Interventions', () => {
        navBarPage.goToEntity('intervention');
        interventionComponentsPage = new InterventionComponentsPage();
        expect(interventionComponentsPage.getTitle()).toMatch(/trialslinkApp.intervention.home.title/);

    });

    it('should load create Intervention dialog', () => {
        interventionComponentsPage.clickOnCreateButton();
        interventionDialogPage = new InterventionDialogPage();
        expect(interventionDialogPage.getModalTitle()).toMatch(/trialslinkApp.intervention.home.createOrEditLabel/);
        interventionDialogPage.close();
    });

    it('should create and save Interventions', () => {
        interventionComponentsPage.clickOnCreateButton();
        interventionDialogPage.setLabelInput('label');
        expect(interventionDialogPage.getLabelInput()).toMatch('label');
        interventionDialogPage.setSystemInput('system');
        expect(interventionDialogPage.getSystemInput()).toMatch('system');
        interventionDialogPage.setCodeInput('code');
        expect(interventionDialogPage.getCodeInput()).toMatch('code');
        interventionDialogPage.setTypeInput('type');
        expect(interventionDialogPage.getTypeInput()).toMatch('type');
        interventionDialogPage.save();
        expect(interventionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class InterventionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-intervention div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class InterventionDialogPage {
    modalTitle = element(by.css('h4#myInterventionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    labelInput = element(by.css('input#field_label'));
    systemInput = element(by.css('input#field_system'));
    codeInput = element(by.css('input#field_code'));
    typeInput = element(by.css('input#field_type'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setLabelInput = function(label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function() {
        return this.labelInput.getAttribute('value');
    }

    setSystemInput = function(system) {
        this.systemInput.sendKeys(system);
    }

    getSystemInput = function() {
        return this.systemInput.getAttribute('value');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    }

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    }

    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    }

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
