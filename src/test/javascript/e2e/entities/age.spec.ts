import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Age e2e test', () => {

    let navBarPage: NavBarPage;
    let ageDialogPage: AgeDialogPage;
    let ageComponentsPage: AgeComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Ages', () => {
        navBarPage.goToEntity('age');
        ageComponentsPage = new AgeComponentsPage();
        expect(ageComponentsPage.getTitle()).toMatch(/trialslinkApp.age.home.title/);

    });

    it('should load create Age dialog', () => {
        ageComponentsPage.clickOnCreateButton();
        ageDialogPage = new AgeDialogPage();
        expect(ageDialogPage.getModalTitle()).toMatch(/trialslinkApp.age.home.createOrEditLabel/);
        ageDialogPage.close();
    });

    it('should create and save Ages', () => {
        ageComponentsPage.clickOnCreateButton();
        ageDialogPage.setValueInput('5');
        expect(ageDialogPage.getValueInput()).toMatch('5');
        ageDialogPage.unitSelectLastOption();
        ageDialogPage.save();
        expect(ageDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AgeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-age div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AgeDialogPage {
    modalTitle = element(by.css('h4#myAgeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    valueInput = element(by.css('input#field_value'));
    unitSelect = element(by.css('select#field_unit'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setValueInput = function(value) {
        this.valueInput.sendKeys(value);
    }

    getValueInput = function() {
        return this.valueInput.getAttribute('value');
    }

    setUnitSelect = function(unit) {
        this.unitSelect.sendKeys(unit);
    }

    getUnitSelect = function() {
        return this.unitSelect.element(by.css('option:checked')).getText();
    }

    unitSelectLastOption = function() {
        this.unitSelect.all(by.tagName('option')).last().click();
    }
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
