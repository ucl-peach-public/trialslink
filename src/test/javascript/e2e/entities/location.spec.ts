import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Location e2e test', () => {

    let navBarPage: NavBarPage;
    let locationDialogPage: LocationDialogPage;
    let locationComponentsPage: LocationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Locations', () => {
        navBarPage.goToEntity('location');
        locationComponentsPage = new LocationComponentsPage();
        expect(locationComponentsPage.getTitle()).toMatch(/trialslinkApp.location.home.title/);

    });

    it('should load create Location dialog', () => {
        locationComponentsPage.clickOnCreateButton();
        locationDialogPage = new LocationDialogPage();
        expect(locationDialogPage.getModalTitle()).toMatch(/trialslinkApp.location.home.createOrEditLabel/);
        locationDialogPage.close();
    });

    it('should create and save Locations', () => {
        locationComponentsPage.clickOnCreateButton();
        locationDialogPage.setNameInput('name');
        expect(locationDialogPage.getNameInput()).toMatch('name');
        locationDialogPage.statusSelectLastOption();
        locationDialogPage.setDescriptionInput('description');
        expect(locationDialogPage.getDescriptionInput()).toMatch('description');
        locationDialogPage.modeSelectLastOption();
        locationDialogPage.setTypeInput('type');
        expect(locationDialogPage.getTypeInput()).toMatch('type');
        locationDialogPage.setPhysicalTypeInput('physicalType');
        expect(locationDialogPage.getPhysicalTypeInput()).toMatch('physicalType');
        locationDialogPage.organisationSelectLastOption();
        locationDialogPage.save();
        expect(locationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class LocationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-location div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class LocationDialogPage {
    modalTitle = element(by.css('h4#myLocationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    statusSelect = element(by.css('select#field_status'));
    descriptionInput = element(by.css('input#field_description'));
    modeSelect = element(by.css('select#field_mode'));
    typeInput = element(by.css('input#field_type'));
    physicalTypeInput = element(by.css('input#field_physicalType'));
    organisationSelect = element(by.css('select#field_organisation'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    }

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    }

    setModeSelect = function(mode) {
        this.modeSelect.sendKeys(mode);
    }

    getModeSelect = function() {
        return this.modeSelect.element(by.css('option:checked')).getText();
    }

    modeSelectLastOption = function() {
        this.modeSelect.all(by.tagName('option')).last().click();
    }
    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    }

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    }

    setPhysicalTypeInput = function(physicalType) {
        this.physicalTypeInput.sendKeys(physicalType);
    }

    getPhysicalTypeInput = function() {
        return this.physicalTypeInput.getAttribute('value');
    }

    organisationSelectLastOption = function() {
        this.organisationSelect.all(by.tagName('option')).last().click();
    }

    organisationSelectOption = function(option) {
        this.organisationSelect.sendKeys(option);
    }

    getOrganisationSelect = function() {
        return this.organisationSelect;
    }

    getOrganisationSelectedOption = function() {
        return this.organisationSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
