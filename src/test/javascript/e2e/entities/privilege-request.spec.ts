import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

export class PrivilegeRequestComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-privilege-request div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PrivilegeRequestDialogPage {
    modalTitle = element(by.css('h4#myPrivilegeRequestLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    statusSelect = element(by.css('select#field_status'));
    created_byInput = element(by.css('input#field_created_by'));
    updated_byInput = element(by.css('input#field_updated_by'));
    created_atInput = element(by.css('input#field_created_at'));
    updated_atInput = element(by.css('input#field_updated_at'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setCreated_byInput = function(created_by) {
        this.created_byInput.sendKeys(created_by);
    }

    getCreated_byInput = function() {
        return this.created_byInput.getAttribute('value');
    }

    setUpdated_byInput = function(updated_by) {
        this.updated_byInput.sendKeys(updated_by);
    }

    getUpdated_byInput = function() {
        return this.updated_byInput.getAttribute('value');
    }

    setCreated_atInput = function(created_at) {
        this.created_atInput.sendKeys(created_at);
    }

    getCreated_atInput = function() {
        return this.created_atInput.getAttribute('value');
    }

    setUpdated_atInput = function(updated_at) {
        this.updated_atInput.sendKeys(updated_at);
    }

    getUpdated_atInput = function() {
        return this.updated_atInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

describe('PrivilegeRequest e2e test', () => {

    let navBarPage: NavBarPage;
    let privilegeRequestDialogPage: PrivilegeRequestDialogPage;
    let privilegeRequestComponentsPage: PrivilegeRequestComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PrivilegeRequests', () => {
        navBarPage.goToEntity('privilege-request');
        privilegeRequestComponentsPage = new PrivilegeRequestComponentsPage();
        expect(privilegeRequestComponentsPage.getTitle()).toMatch(/trialslinkApp.privilegeRequest.home.title/);
    });

    it('should load create PrivilegeRequest dialog', () => {
//<<<<<<< HEAD
//        element(by.css('button.create-privilege-request')).click().then(() => {
//            const expectVal = /trialslinkApp.privilegeRequest.home.createOrEditLabel/;
//            element.all(by.css('h4.modal-title')).first().getAttribute('jhiTranslate').then((value) => {
//                expect(value).toMatch(expectVal);
//            });
//=======
        privilegeRequestComponentsPage.clickOnCreateButton();
        privilegeRequestDialogPage = new PrivilegeRequestDialogPage();
        expect(privilegeRequestDialogPage.getModalTitle()).toMatch(/trialslinkApp.privilegeRequest.home.createOrEditLabel/);
        privilegeRequestDialogPage.close();
    });
//>>>>>>> jhipster_upgrade

    it('should create and save PrivilegeRequests', () => {
        privilegeRequestComponentsPage.clickOnCreateButton();
        privilegeRequestDialogPage.statusSelectLastOption();
        privilegeRequestDialogPage.setCreated_byInput('created_by');
        expect(privilegeRequestDialogPage.getCreated_byInput()).toMatch('created_by');
        privilegeRequestDialogPage.setUpdated_byInput('updated_by');
        expect(privilegeRequestDialogPage.getUpdated_byInput()).toMatch('updated_by');
        privilegeRequestDialogPage.setCreated_atInput(12310020012301);
        expect(privilegeRequestDialogPage.getCreated_atInput()).toMatch('2001-12-31T02:30');
        privilegeRequestDialogPage.setUpdated_atInput(12310020012301);
        expect(privilegeRequestDialogPage.getUpdated_atInput()).toMatch('2001-12-31T02:30');
        privilegeRequestDialogPage.save();
        expect(privilegeRequestDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
//<<<<<<< HEAD
//        accountMenu.click();
//        logout.click();
//=======
        navBarPage.autoSignOut();
//>>>>>>> jhipster_upgrade
    });
});
