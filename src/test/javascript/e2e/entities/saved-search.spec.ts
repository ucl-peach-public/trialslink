import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

export class SavedSearchComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-saved-search div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SavedSearchDialogPage {
    modalTitle = element(by.css('h4#mySavedSearchLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    createdAtInput = element(by.css('input#field_createdAt'));
    updatedAtInput = element(by.css('input#field_updatedAt'));
    urlInput = element(by.css('input#field_url'));
    hashInput = element(by.css('input#field_hash'));
    createdByInput = element(by.css('input#field_createdBy'));
    shouldNotifyInput = element(by.css('input#field_shouldNotify'));
    titleInput = element(by.css('input#field_title'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCreatedAtInput = function(createdAt) {
        this.createdAtInput.sendKeys(createdAt);
    }

    getCreatedAtInput = function() {
        return this.createdAtInput.getAttribute('value');
    }

    setUpdatedAtInput = function(updatedAt) {
        this.updatedAtInput.sendKeys(updatedAt);
    }

    getUpdatedAtInput = function() {
        return this.updatedAtInput.getAttribute('value');
    }

    setUrlInput = function(url) {
        this.urlInput.sendKeys(url);
    }

    getUrlInput = function() {
        return this.urlInput.getAttribute('value');
    }

    setHashInput = function(hash) {
        this.hashInput.sendKeys(hash);
    }

    getHashInput = function() {
        return this.hashInput.getAttribute('value');
    }

    setCreatedByInput = function(createdBy) {
        this.createdByInput.sendKeys(createdBy);
    }

    getCreatedByInput = function() {
        return this.createdByInput.getAttribute('value');
    }

    getShouldNotifyInput = function() {
        return this.shouldNotifyInput;
    }
    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    }

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

describe('SavedSearch e2e test', () => {

    let navBarPage: NavBarPage;
    let savedSearchDialogPage: SavedSearchDialogPage;
    let savedSearchComponentsPage: SavedSearchComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SavedSearches', () => {
        navBarPage.goToEntity('saved-search');
        savedSearchComponentsPage = new SavedSearchComponentsPage();
        expect(savedSearchComponentsPage.getTitle()).toMatch(/trialslinkApp.savedSearch.home.title/);

    });

    it('should load create SavedSearch dialog', () => {
//<<<<<<< HEAD
//        element(by.css('button.create-saved-search')).click().then(() => {
//            const expectVal = /trialslinkApp.savedSearch.home.createOrEditLabel/;
//            element.all(by.css('h4.modal-title')).first().getAttribute('jhiTranslate').then((value) => {
//                expect(value).toMatch(expectVal);
//            });
//=======
        savedSearchComponentsPage.clickOnCreateButton();
        savedSearchDialogPage = new SavedSearchDialogPage();
        expect(savedSearchDialogPage.getModalTitle()).toMatch(/trialslinkApp.savedSearch.home.createOrEditLabel/);
        savedSearchDialogPage.close();
    });
//>>>>>>> jhipster_upgrade

    it('should create and save SavedSearches', () => {
        savedSearchComponentsPage.clickOnCreateButton();
        savedSearchDialogPage.setCreatedAtInput(12310020012301);
        expect(savedSearchDialogPage.getCreatedAtInput()).toMatch('2001-12-31T02:30');
        savedSearchDialogPage.setUpdatedAtInput(12310020012301);
        expect(savedSearchDialogPage.getUpdatedAtInput()).toMatch('2001-12-31T02:30');
        savedSearchDialogPage.setUrlInput('url');
        expect(savedSearchDialogPage.getUrlInput()).toMatch('url');
        savedSearchDialogPage.setHashInput('hash');
        expect(savedSearchDialogPage.getHashInput()).toMatch('hash');
        savedSearchDialogPage.setCreatedByInput('createdBy');
        expect(savedSearchDialogPage.getCreatedByInput()).toMatch('createdBy');
        savedSearchDialogPage.getShouldNotifyInput().isSelected().then((selected) => {
            if (selected) {
                savedSearchDialogPage.getShouldNotifyInput().click();
                expect(savedSearchDialogPage.getShouldNotifyInput().isSelected()).toBeFalsy();
            } else {
                savedSearchDialogPage.getShouldNotifyInput().click();
                expect(savedSearchDialogPage.getShouldNotifyInput().isSelected()).toBeTruthy();
            }
        });
        savedSearchDialogPage.setTitleInput('title');
        expect(savedSearchDialogPage.getTitleInput()).toMatch('title');
        savedSearchDialogPage.save();
        expect(savedSearchDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
//<<<<<<< HEAD
//        accountMenu.click();
//        logout.click();
//=======
        navBarPage.autoSignOut();
//>>>>>>> jhipster_upgrade
    });
});
